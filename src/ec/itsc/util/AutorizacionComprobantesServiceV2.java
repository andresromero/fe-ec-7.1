package ec.itsc.util;

import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.WebEndpoint;

import ec.gob.sri.comprobantes.ws.aut.AutorizacionComprobantes;
import ec.gob.sri.comprobantes.ws.aut.AutorizacionComprobantesService;

public class AutorizacionComprobantesServiceV2 extends AutorizacionComprobantesService{
	
	public boolean isOffline;
	
	public AutorizacionComprobantesServiceV2(URL wsdlLocation, QName serviceName, boolean isOffline) {
		super(wsdlLocation, serviceName);
		this.isOffline = isOffline;
	}
	
	  
	@WebEndpoint(name="AutorizacionComprobantesOfflinePort")
	public AutorizacionComprobantes getAutorizacionComprobantesOfflinePort()
	{
	  return (AutorizacionComprobantes)super.getPort(new QName("http://ec.gob.sri.ws.autorizacion", "AutorizacionComprobantesOfflinePort"), AutorizacionComprobantes.class);
	}
	
	public AutorizacionComprobantes getAutorizacionComprobantesPort()
	{
		//if(isOffline) //ahora solo trabaja modo offline CAMC
			return getAutorizacionComprobantesOfflinePort();
		//else
		//	return super.getAutorizacionComprobantesPort();
	}

}
