package ec.itsc.util;

import java.io.File;
import java.util.List;
import java.util.Properties;

import org.compiere.model.MClient;
import org.compiere.model.MMailText;
import org.compiere.model.MOrgInfo;
import org.compiere.model.MUser;
import org.compiere.model.MUserMail;
import org.compiere.util.EMail;
import org.compiere.util.Env;

public class Util {

	public static EMail createEMail (MClient client, String from, String to, String subject, String message, boolean html)
	{
		if (to == null || to.length() == 0)
		{
			System.out.println("no to");
			return null;
		}
		//
		EMail email = new EMail (client, from, to, subject, message, html);
		if (client.isSmtpAuthorization())
			email.createAuthenticator (client.getRequestUser(), client.getRequestUserPW());
		return email;
	}	//	createEMail
	
	public static int notifyUsers(Properties ctx, MOrgInfo orginfo, MMailText mText, int ad_user_id, String subject, String message, List<File> attachments,  String trxName, int ad_user2_id)
	{
		int countMail = 0;
		Boolean invalidUser = false;

		if(ad_user_id<1)
			ad_user_id = ad_user2_id;

		if (ad_user_id > 0){
			MUser notifyTo = new MUser (ctx, ad_user_id, trxName);
			//MClient client = new MClient (ctx, Env.getAD_Client_ID(ctx), trxName);
			MClient client = new MClient (ctx, notifyTo.getAD_Client_ID(), trxName);
			
			MUser bcc = null;

			if(!isValidMail(notifyTo))
				invalidUser = true;//notifyTo = new MUser (ctx, ad_user2_id, trxName);
			
			if (ad_user2_id > 0)
				bcc = new MUser(ctx, ad_user2_id, trxName);
			
			if (!invalidUser || isValidMail(bcc)) {
				
				String msg = null;
				
				EMail email = createEMail(client, orginfo.get_ValueAsString("email_envios"), invalidUser?bcc.getEMail():notifyTo.getEMail(), subject, message,false);
				
				if (email.isValid()) {
					for (File att : attachments) {
						email.addAttachment(att);
					}
					
					if (bcc != null){
						if (bcc.isNotificationEMail())
							email.addCc(bcc.getEMail());
					}
					
					for(MUser copia : MUser.getOfBPartner(Env.getCtx(), notifyTo.getC_BPartner_ID(), notifyTo.get_TrxName())){
						if(copia.isEMailValid() && copia.get_ValueAsBoolean("esCopiaDE"))
							email.addCc(copia.getEMail());
					}
					
					msg = email.send();
				}
				
				MUserMail um = new MUserMail(mText, ad_user_id, email);
				um.setSubject(subject);
				um.setMailText(message);
				um.saveEx();
				
				if (msg.equals(EMail.SENT_OK))
				{
					countMail++;
				}
			}
		}

		return countMail;
	}

	private static Boolean isValidMail(MUser user){
		return user!=null && user.getEMail()!=null && user.getEMail().length()>0 && user.isEMailValid() && user.isNotificationEMail();
	}
}
