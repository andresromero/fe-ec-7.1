package ec.itsc.util;

import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.WebEndpoint;

import ec.gob.sri.comprobantes.ws.RecepcionComprobantes;
import ec.gob.sri.comprobantes.ws.RecepcionComprobantesService;


public class RecepcionComprobantesServiceV2 extends RecepcionComprobantesService{
	
	boolean isOffline;
	
	public RecepcionComprobantesServiceV2(URL wsdlLocation, QName serviceName, boolean isOffline) {
		super(wsdlLocation, serviceName);
		this.isOffline = isOffline;
	}
	
	@WebEndpoint(name="RecepcionComprobantesOfflinePort")
	public RecepcionComprobantes getRecepcionComprobantesOfflinePort()
	{
		return (RecepcionComprobantes)super.getPort(new QName("http://ec.gob.sri.ws.recepcion", "RecepcionComprobantesOfflinePort"), RecepcionComprobantes.class);
    }

	public RecepcionComprobantes getRecepcionComprobantesPort()
	{
		//if(isOffline) //ahora solo trabaja modo offline CAMC
			return getRecepcionComprobantesOfflinePort();
		//else
		//	return super.getRecepcionComprobantesPort();
    }

}
