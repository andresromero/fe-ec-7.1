//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.8-b130911.1802 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2019.12.04 a las 10:37:15 PM COT 
//


package ec.itsc.liquidacionCompras;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the generated package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: generated
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link LiquidacionCompra }
     * 
     */
    public LiquidacionCompra createLiquidacionCompra() {
        return new LiquidacionCompra();
    }

    /**
     * Create an instance of {@link Pagos }
     * 
     */
    public Pagos createPagos() {
        return new Pagos();
    }

    /**
     * Create an instance of {@link DetalleImpuestos }
     * 
     */
    public DetalleImpuestos createDetalleImpuestos() {
        return new DetalleImpuestos();
    }

    /**
     * Create an instance of {@link LiquidacionCompra.InfoAdicional }
     * 
     */
    public LiquidacionCompra.InfoAdicional createLiquidacionCompraInfoAdicional() {
        return new LiquidacionCompra.InfoAdicional();
    }

    /**
     * Create an instance of {@link Reembolsos }
     * 
     */
    public Reembolsos createReembolsos() {
        return new Reembolsos();
    }

    /**
     * Create an instance of {@link LiquidacionCompra.Detalles }
     * 
     */
    public LiquidacionCompra.Detalles createLiquidacionCompraDetalles() {
        return new LiquidacionCompra.Detalles();
    }

    /**
     * Create an instance of {@link LiquidacionCompra.Detalles.Detalle }
     * 
     */
    public LiquidacionCompra.Detalles.Detalle createLiquidacionCompraDetallesDetalle() {
        return new LiquidacionCompra.Detalles.Detalle();
    }

    /**
     * Create an instance of {@link LiquidacionCompra.Detalles.Detalle.DetallesAdicionales }
     * 
     */
    public LiquidacionCompra.Detalles.Detalle.DetallesAdicionales createLiquidacionCompraDetallesDetalleDetallesAdicionales() {
        return new LiquidacionCompra.Detalles.Detalle.DetallesAdicionales();
    }

    /**
     * Create an instance of {@link LiquidacionCompra.InfoLiquidacionCompra }
     * 
     */
    public LiquidacionCompra.InfoLiquidacionCompra createLiquidacionCompraInfoLiquidacionCompra() {
        return new LiquidacionCompra.InfoLiquidacionCompra();
    }

    /**
     * Create an instance of {@link LiquidacionCompra.InfoLiquidacionCompra.TotalConImpuestos }
     * 
     */
    public LiquidacionCompra.InfoLiquidacionCompra.TotalConImpuestos createLiquidacionCompraInfoLiquidacionCompraTotalConImpuestos() {
        return new LiquidacionCompra.InfoLiquidacionCompra.TotalConImpuestos();
    }

    /**
     * Create an instance of {@link InfoTributaria }
     * 
     */
    public InfoTributaria createInfoTributaria() {
        return new InfoTributaria();
    }

    /**
     * Create an instance of {@link TipoNegociable }
     * 
     */
    public TipoNegociable createTipoNegociable() {
        return new TipoNegociable();
    }

    /**
     * Create an instance of {@link MaquinaFiscal }
     * 
     */
    public MaquinaFiscal createMaquinaFiscal() {
        return new MaquinaFiscal();
    }

    /**
     * Create an instance of {@link Impuesto }
     * 
     */
    public Impuesto createImpuesto() {
        return new Impuesto();
    }

    /**
     * Create an instance of {@link Pagos.Pago }
     * 
     */
    public Pagos.Pago createPagosPago() {
        return new Pagos.Pago();
    }

    /**
     * Create an instance of {@link DetalleImpuestos.DetalleImpuesto }
     * 
     */
    public DetalleImpuestos.DetalleImpuesto createDetalleImpuestosDetalleImpuesto() {
        return new DetalleImpuestos.DetalleImpuesto();
    }

    /**
     * Create an instance of {@link LiquidacionCompra.InfoAdicional.CampoAdicional }
     * 
     */
    public LiquidacionCompra.InfoAdicional.CampoAdicional createLiquidacionCompraInfoAdicionalCampoAdicional(String nombre, String value) {
        return new LiquidacionCompra.InfoAdicional.CampoAdicional(nombre, value);
    }

    /**
     * Create an instance of {@link Reembolsos.ReembolsoDetalle }
     * 
     */
    public Reembolsos.ReembolsoDetalle createReembolsosReembolsoDetalle() {
        return new Reembolsos.ReembolsoDetalle();
    }

    /**
     * Create an instance of {@link LiquidacionCompra.Detalles.Detalle.Impuestos }
     * 
     */
    public LiquidacionCompra.Detalles.Detalle.Impuestos createLiquidacionCompraDetallesDetalleImpuestos() {
        return new LiquidacionCompra.Detalles.Detalle.Impuestos();
    }

    /**
     * Create an instance of {@link LiquidacionCompra.Detalles.Detalle.DetallesAdicionales.DetAdicional }
     * 
     */
    public LiquidacionCompra.Detalles.Detalle.DetallesAdicionales.DetAdicional createLiquidacionCompraDetallesDetalleDetallesAdicionalesDetAdicional(String nombre, String valor) {
        return new LiquidacionCompra.Detalles.Detalle.DetallesAdicionales.DetAdicional(nombre, valor);
    }

    /**
     * Create an instance of {@link LiquidacionCompra.InfoLiquidacionCompra.TotalConImpuestos.TotalImpuesto }
     * 
     */
    public LiquidacionCompra.InfoLiquidacionCompra.TotalConImpuestos.TotalImpuesto createLiquidacionCompraInfoLiquidacionCompraTotalConImpuestosTotalImpuesto() {
        return new LiquidacionCompra.InfoLiquidacionCompra.TotalConImpuestos.TotalImpuesto();
    }

}
