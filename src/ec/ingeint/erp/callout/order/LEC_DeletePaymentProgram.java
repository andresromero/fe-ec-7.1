package ec.ingeint.erp.callout.order;

import java.util.Properties;

import org.adempiere.base.IColumnCallout;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.model.MInvoice;
import org.compiere.model.MInvoicePaySchedule;

public class LEC_DeletePaymentProgram implements IColumnCallout{

	@Override
	public String start(Properties ctx, int WindowNo, GridTab mTab,
			GridField mField, Object value, Object oldValue) {

		String PaymentRule = mTab.get_ValueAsString("PaymentRule");
		
		if(!PaymentRule.equals(MInvoice.PAYMENTRULE_OnCredit)){
			MInvoicePaySchedule[] ips = MInvoicePaySchedule.getInvoicePaySchedule(ctx, mTab.getRecord_ID(), 0, null);
			
			int rows = ips.length;
			
			for (int i = 0; i < rows; i++){
				ips[i].deleteEx(true);
			}
			mTab.setValue(MInvoice.COLUMNNAME_IsPayScheduleValid, false);
		}
		return null;
	}

}