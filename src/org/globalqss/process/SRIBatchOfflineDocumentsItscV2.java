package org.globalqss.process;

import java.sql.Timestamp;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MInvoice;
import org.compiere.model.Query;
import org.compiere.model.MCost.QtyCost;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.Env;
import org.globalqss.model.X_SRI_Authorization;
import org.globalqss.util.LEC_FE_ProcessOnThread;

public class SRIBatchOfflineDocumentsItscV2 extends SvrProcess {

	/** Invoice = 01 */
	public static final String SRI_ShortDocType_Invoice = "01";
	/** Liquidacion Compra = 03 */
	public static final String SRI_ShortDocType_LiquidacionCompra = "03";
	/** Credit Memo = 04 */
	public static final String SRI_ShortDocType_CreditMemo = "04";
	/** Debit Memo = 05 */
	public static final String SRI_ShortDocType_DebitMemo = "05";
	/** Shipment = 06 */
	public static final String SRI_ShortDocType_Shipment = "06";
	/** Withholding = 07 */
	public static final String SRI_ShortDocType_Withholding = "07";
	

	Integer p_org_id = null;
	Boolean p_rangofecha = null;
	Integer p_invoice_id = null;
	Timestamp p_DateFrom = null;
	Timestamp p_DateTo = null;
	String p_tipo_identificacion = null;
	
	Boolean p_enviarreprocesar = null;
	
	Integer p_Qty = null;
	String sriShortDocType = "";
	

	@Override
	protected void prepare() {
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++) {
			String name = para[i].getParameterName();
			if (name.equals("C_Invoice_ID"))
				p_invoice_id = para[i].getParameterAsInt();
			else if (name.equals("AD_Org_ID"))
				p_org_id = para[i].getParameterAsInt();
			else if (name.equals("rangofecha"))
				p_rangofecha = para[i].getParameterAsBoolean();
			else if (name.equals("enviarreprocesar"))
				p_enviarreprocesar = para[i].getParameterAsBoolean();
			else if (name.equals("DateFrom"))
				p_DateFrom = para[i].getParameterAsTimestamp();
			else if (name.equals("DateTo"))
				p_DateTo = para[i].getParameterAsTimestamp();
			else if (name.equals("Qty"))
				p_Qty = para[i].getParameterAsInt();
			else if (name.equals("SRI_ShortDocType"))
				sriShortDocType = para[i].getParameterAsString();
			else if (name.equals("LEC_TaxCodeSRI"))
				p_tipo_identificacion = para[i].getParameterAsString();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
	}

	@Override
	protected String doIt() throws Exception {
		String result = "";
		
		if (sriShortDocType.equals(SRI_ShortDocType_Invoice)) {// Process Autorization
			result = envioFacturasVenta();
		} 
		else if(sriShortDocType.equals(SRI_ShortDocType_LiquidacionCompra)){
			//result = envioLiquidacionCompra();
		}
		else if(sriShortDocType.equals("03-07")){}
		else if(sriShortDocType.equals(SRI_ShortDocType_CreditMemo)){}
		else if(sriShortDocType.equals(SRI_ShortDocType_DebitMemo)){}
		else if(sriShortDocType.equals(SRI_ShortDocType_Shipment)){}
		else if(sriShortDocType.equals(SRI_ShortDocType_Withholding)){}
		else envioComprobantes();
		
		return result;
	}


	private String envioFacturasVenta(){
		if(p_rangofecha){
			if (p_DateFrom == null || p_DateTo == null) 
				throw new AdempiereException("Ingresar rango de facturas");
		}
		else 
			if(p_invoice_id == null)
				throw new AdempiereException("Ingresar factura a procesar");
		
		String whereSql = "/*SRI_AuthorizationCode Is NUll AND*/ (SRI_ErrorCode_ID IS NULL OR SRI_ErrorCode_ID = "
				+ "(Select ec.SRI_ErrorCode_ID FROM SRI_ErrorCode ec WHERE ec.Value ='70' and ad_client_id = SRI_Authorization.ad_client_id)) "
				+ "AND SRI_ShortDocType = '01' AND Value IS NOT NULL "
				+ "AND EXISTS (Select acc.SRI_AccessCode_ID FROM SRI_AccessCode acc WHERE acc.SRI_AccessCode_ID = SRI_Authorization.SRI_AccessCode_ID AND acc.CodeAccessType = '1') "
				+ "AND (case when ?=0 then (Created>= ? and Created <= ?) else (Select SRI_Authorization_ID from c_invoice where c_invoice_id = ?)=SRI_Authorization.SRI_Authorization_ID end) ";
		
		whereSql+=" AND Processed='N' AND isSRIOfflineSchema ='Y' AND ? in (AD_Org_ID,0) and "+Env.getAD_Client_ID(Env.getCtx())+" in (ad_client_id,0) ";
		if(p_tipo_identificacion==null || p_tipo_identificacion.length()==0)
			whereSql+= "AND EXISTS (Select 1 from c_invoice ci where ci.SRI_Authorization_ID = SRI_Authorization.SRI_Authorization_ID and ci.docstatus in ('CO','CL'))";//CAMC
		else
			whereSql+= "AND EXISTS (Select 1 from c_invoice ci join C_BPartner using(C_BPartner_ID) join LCO_TaxIdType using(LCO_TaxIdType_ID) where ci.SRI_Authorization_ID = SRI_Authorization.SRI_Authorization_ID and ci.docstatus in ('CO','CL') and LEC_TaxCodeSRI='"+p_tipo_identificacion+"')";//CAMC
		
		List<X_SRI_Authorization> autorizations = new Query(getCtx(),
				X_SRI_Authorization.Table_Name, whereSql, get_TrxName())
				.setOnlyActiveRecords(true).setParameters(p_rangofecha?0:1,p_DateFrom, p_DateTo, p_invoice_id,p_org_id)
				.setOrderBy("coalesce(LastSynchronized,'1900-01-01') asc, created").list();
		
		if(p_Qty!=null /*&& p_Qty>0*/ && autorizations.size()>p_Qty){
			autorizations = autorizations.subList(0, p_Qty);
		}
		
		if (autorizations == null) {
			log.warning("No unprocessed Autorization on MInvoice ");
			return "No unprocessed Autorization on MInvoice";
		}

		int count = 0;
		Vector<Thread> threadAutorizations = new Vector<Thread>();
		for (X_SRI_Authorization autorization : autorizations) {
			autorization.set_ValueOfColumn("LastSynchronized", new Timestamp(System.currentTimeMillis()));
			autorization.saveEx();
			Integer autorizationID = autorization.getSRI_Authorization_ID();
			int ids[] = MInvoice.getAllIDs(MInvoice.Table_Name, "docstatus in ('CO','CL') and SRI_Authorization_ID="+autorizationID, get_TrxName());
			if(ids.length>1){
				addLog(autorizationID, null, null,
						" \nAutorización no programada: multiples facturas de venta configuradas-> " + autorization.getValue());
				continue;
			}
			else if(ids.length==0){
				addLog(autorizationID, null, null,
						" \nAutorización no programada: No se encontro factura correctamente asociada-> " + autorization.getValue());
				continue;
			}
			
			LEC_FE_ProcessOnThread tautorization = new LEC_FE_ProcessOnThread(
					getCtx(), p_enviarreprocesar?ids[0]:autorizationID,
					LEC_FE_ProcessOnThread.SRI_ShortDocType_Invoice, p_enviarreprocesar?"01OF":"01RP",//OFFLINE
					autorization.getAD_UserMail_ID(),
					Env.getProcessInfo(getCtx()));
			tautorization.start();
			threadAutorizations.add(tautorization);
			count++;
			addLog(autorizationID, null, null,
					" \nAutorización-> " + autorization.getValue());
		}
		return "Se Programaron " + count+ " Autorizaciones de Facturas de venta para su aprobación\n";
	}
	

	private String envioLiquidacionCompra(){
		if(p_rangofecha){
			if (p_DateFrom == null || p_DateTo == null) 
				throw new AdempiereException("Ingresar rango de Liquidación de Compras");
		}
		else 
			if(p_invoice_id == null)
				throw new AdempiereException("Ingresar Liquidación de Compras a procesar");
		
		String whereSql = "/*SRI_AuthorizationCode Is NUll AND*/ (SRI_ErrorCode_ID IS NULL OR SRI_ErrorCode_ID = "
				+ "(Select ec.SRI_ErrorCode_ID FROM SRI_ErrorCode ec WHERE ec.Value ='70' and ad_client_id = SRI_Authorization.ad_client_id)) "
				+ "AND SRI_ShortDocType = '03' AND Value IS NOT NULL "
				+ "AND EXISTS (Select acc.SRI_AccessCode_ID FROM SRI_AccessCode acc WHERE acc.SRI_AccessCode_ID = SRI_Authorization.SRI_AccessCode_ID AND acc.CodeAccessType = '1') "
				+ "AND (case when ?=0 then (Created>= ? and Created <= ?) else (Select sri_authorization_invoice_ID from c_invoice where c_invoice_id = ?)=SRI_Authorization.SRI_Authorization_ID end) ";
		
		whereSql+=" AND Processed='N' AND isSRIOfflineSchema ='Y' AND ? in (AD_Org_ID,0) and "+Env.getAD_Client_ID(Env.getCtx())+" in (ad_client_id,0) ";
		if(p_tipo_identificacion==null || p_tipo_identificacion.length()==0)
			whereSql+= "AND EXISTS (Select 1 from c_invoice ci where ci.sri_authorization_invoice_ID = SRI_Authorization.SRI_Authorization_ID and ci.docstatus in ('CO','CL'))";//CAMC
		else
			whereSql+= "AND EXISTS (Select 1 from c_invoice ci join C_BPartner using(C_BPartner_ID) join LCO_TaxIdType using(LCO_TaxIdType_ID) where ci.sri_authorization_invoice_ID = SRI_Authorization.SRI_Authorization_ID and ci.docstatus in ('CO','CL') and LEC_TaxCodeSRI='"+p_tipo_identificacion+"')";//CAMC
		
		List<X_SRI_Authorization> autorizations = new Query(getCtx(),
				X_SRI_Authorization.Table_Name, whereSql, get_TrxName())
				.setOnlyActiveRecords(true).setParameters(p_rangofecha?0:1,p_DateFrom, p_DateTo, p_invoice_id,p_org_id)
				.setOrderBy("coalesce(LastSynchronized,'1900-01-01') asc, created").list();
		
		if(p_Qty!=null /*&& p_Qty>0*/ && autorizations.size()>p_Qty){
			autorizations = autorizations.subList(0, p_Qty);
		}
		
		if (autorizations == null) {
			log.warning("No unprocessed Autorization on MInvoice LC ");
			return "No unprocessed Autorization on MInvoice LC";
		}

		int count = 0;
		Vector<Thread> threadAutorizations = new Vector<Thread>();
		for (X_SRI_Authorization autorization : autorizations) {
			autorization.set_ValueOfColumn("LastSynchronized", new Timestamp(System.currentTimeMillis()));
			autorization.saveEx();
			Integer autorizationID = autorization.getSRI_Authorization_ID();
			int ids[] = MInvoice.getAllIDs(MInvoice.Table_Name, "docstatus in ('CO','CL') and sri_authorization_invoice_ID="+autorizationID, get_TrxName());
			if(ids.length>1){
				addLog(autorizationID, null, null,
						" \nAutorización no programada: multiples Liquidación de Compras de venta configuradas-> " + autorization.getValue());
				continue;
			}
			else if(ids.length==0){
				addLog(autorizationID, null, null,
						" \nAutorización no programada: No se encontro Liquidación de Compra correctamente asociada-> " + autorization.getValue());
				continue;
			}
			
			LEC_FE_ProcessOnThread tautorization = new LEC_FE_ProcessOnThread(
					getCtx(), p_enviarreprocesar?ids[0]:autorizationID,
					LEC_FE_ProcessOnThread.SRI_ShortDocType_LiquidacionCompra, p_enviarreprocesar?"03OF":"03RP",//OFFLINE
					autorization.getAD_UserMail_ID(),
					Env.getProcessInfo(getCtx()));
			tautorization.start();
			threadAutorizations.add(tautorization);
			count++;
			addLog(autorizationID, null, null,
					" \nAutorización-> " + autorization.getValue());
		}
		return "Se Programaron " + count+ " Autorizaciones de Liquidación de Compras de venta para su aprobación\n";
	}
	
	private String envioComprobantes(){
		return envioFacturasVenta();
	}


}
