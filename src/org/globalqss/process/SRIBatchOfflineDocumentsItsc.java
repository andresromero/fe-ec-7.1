package org.globalqss.process;

import java.sql.Timestamp;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;

import org.compiere.model.MInvoice;
import org.compiere.model.Query;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.Env;
import org.globalqss.model.X_SRI_Authorization;
import org.globalqss.util.LEC_FE_ProcessOnThread;

public class SRIBatchOfflineDocumentsItsc extends SvrProcess {
	
	/** Invoice = 01 */
	public static final String SRI_ShortDocType_Invoice = "01";
	/** Credit Memo = 04 */
	public static final String SRI_ShortDocType_CreditMemo = "04";
	/** Debit Memo = 05 */
	public static final String SRI_ShortDocType_DebitMemo = "05";
	/** Shipment = 06 */
	public static final String SRI_ShortDocType_Shipment = "06";
	/** Withholding = 07 */
	public static final String SRI_ShortDocType_Withholding = "07";
	

	Timestamp p_DateTrx = null;
	String sriShortDocType = "";
	

	@Override
	protected void prepare() {
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++) {
			String name = para[i].getParameterName();
			if (name.equals("DateTrx"))
				p_DateTrx = para[i].getParameterAsTimestamp();
			else if (name.equals("SRI_ShortDocType"))
				sriShortDocType = para[i].getParameterAsString();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
	}

	@Override
	protected String doIt() throws Exception {
		String result = "";
		
		if (sriShortDocType.equals(SRI_ShortDocType_Invoice)) {// Process Autorization
			result = envioFacturasVenta();
		} 
		else if(sriShortDocType.equals(SRI_ShortDocType_Invoice)){}
		else if(sriShortDocType.equals(SRI_ShortDocType_CreditMemo)){}
		else if(sriShortDocType.equals(SRI_ShortDocType_DebitMemo)){}
		else if(sriShortDocType.equals(SRI_ShortDocType_Shipment)){}
		else if(sriShortDocType.equals(SRI_ShortDocType_Withholding)){}
		else envioComprobantes();
		
		return result;
	}

	
	private String envioFacturasVenta(){
		if (p_DateTrx == null) {
			p_DateTrx = Env.getContextAsDate(getCtx(), "#Date");
		}

		String whereSql = "/*SRI_AuthorizationCode Is NUll AND*/ (SRI_ErrorCode_ID IS NULL OR SRI_ErrorCode_ID = "
				+ "(Select ec.SRI_ErrorCode_ID FROM SRI_ErrorCode ec WHERE ec.Value ='70')) "
				+ "AND SRI_ShortDocType = '01' AND Value IS NOT NULL "
				//+ "AND AD_UserMail_ID IS NOT NULL "
				+ "AND EXISTS (Select acc.SRI_AccessCode_ID FROM SRI_AccessCode acc WHERE acc.SRI_AccessCode_ID = SRI_Authorization.SRI_AccessCode_ID AND acc.CodeAccessType = '1') "
				+ "AND Date_Trunc('day'::text, Created::timestamp) = ? ";
		
		whereSql+=" AND Processed='N' AND isSRIOfflineSchema ='Y' "
				+ "AND EXISTS (Select * from c_invoice ci where ci.SRI_Authorization_ID = SRI_Authorization.SRI_Authorization_ID )";//CAMC
		
		List<X_SRI_Authorization> autorizations = new Query(getCtx(),
				X_SRI_Authorization.Table_Name, whereSql, get_TrxName())
				.setOnlyActiveRecords(true).setParameters(p_DateTrx)
				.setOrderBy("Created").list();

		if (autorizations == null) {
			log.warning("No unprocessed Autorization on MInvoice ");
			return "No unprocessed Autorization on MInvoice";
		}

		int count = 0;
		Vector<Thread> threadAutorizations = new Vector<Thread>();
		for (X_SRI_Authorization autorization : autorizations) {
			Integer autorizationID = autorization.getSRI_Authorization_ID();
			int ids[] = MInvoice.getAllIDs(MInvoice.Table_Name, "SRI_Authorization_ID="+autorizationID, get_TrxName());
			if(ids.length>1){
				addLog(autorizationID, null, null,
						" \nAutorización no programada: multiples facturas de venta configuradas-> " + autorization.getValue());
				continue;
			}
			
			LEC_FE_ProcessOnThread tautorization = new LEC_FE_ProcessOnThread(
					getCtx(), ids[0],
					LEC_FE_ProcessOnThread.SRI_ShortDocType_Invoice, "01OF",//OFFLINE
					autorization.getAD_UserMail_ID(),
					Env.getProcessInfo(getCtx()));
			tautorization.start();
			threadAutorizations.add(tautorization);
			count++;
			addLog(autorizationID, null, null,
					" \nAutorización-> " + autorization.getValue());
		}
		return "Se Programaron " + count+ " Autorizaciones de Facturas de venta para su aprobación\n";
		
	}
	
	private String envioComprobantes(){
		return envioFacturasVenta();
	}


}
