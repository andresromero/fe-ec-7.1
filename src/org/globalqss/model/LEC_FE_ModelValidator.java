package org.globalqss.model;

import java.io.File;
import java.math.BigDecimal;
import java.util.logging.Level;

import org.adempiere.base.event.AbstractEventHandler;
import org.adempiere.base.event.IEventTopics;
import org.adempiere.exceptions.AdempiereException;
import org.adempiere.util.ProcessUtil;
import org.compiere.model.MAttachment;
import org.compiere.model.MAttachmentEntry;
import org.compiere.model.MBPartner;
import org.compiere.model.MDocType;
import org.compiere.model.MInOut;
import org.compiere.model.MInvoice;
import org.compiere.model.MInvoiceLine;
import org.compiere.model.MInvoicePaySchedule;
import org.compiere.model.MLocation;
import org.compiere.model.MMovement;
import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.model.MOrderPaySchedule;
import org.compiere.model.MOrgInfo;
import org.compiere.model.MPInstance;
import org.compiere.model.MPaySchedule;
import org.compiere.model.MProcess;
import org.compiere.model.MSysConfig;
import org.compiere.model.MTable;
import org.compiere.model.MUser;
import org.compiere.model.PO;
import org.compiere.model.Query;
import org.compiere.process.ProcessInfo;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.Trx;
import org.globalqss.util.LEC_FE_Utils;
import org.osgi.service.event.Event;

import com.sun.security.ntlm.Server;

public class LEC_FE_ModelValidator extends AbstractEventHandler {

	/** Logger */
	private static CLogger log = CLogger
			.getCLogger(LEC_FE_ModelValidator.class);

	@Override
	protected void initialize() {
		// Documents to be monitored
		registerTableEvent(IEventTopics.DOC_BEFORE_COMPLETE,MInvoice.Table_Name);
		registerTableEvent(IEventTopics.DOC_AFTER_COMPLETE, MInvoice.Table_Name);
		registerTableEvent(IEventTopics.DOC_AFTER_COMPLETE, MInOut.Table_Name);
		registerTableEvent(IEventTopics.DOC_AFTER_COMPLETE,	MMovement.Table_Name);
		registerTableEvent(IEventTopics.DOC_AFTER_COMPLETE,	MOrder.Table_Name);
		
		// Tables to be monitored
		registerTableEvent(IEventTopics.PO_BEFORE_NEW, MInvoice.Table_Name);
		registerTableEvent(IEventTopics.PO_BEFORE_NEW, MInOut.Table_Name);
		registerTableEvent(IEventTopics.PO_AFTER_NEW,MLCOInvoiceWithholding.Table_Name);
		registerTableEvent(IEventTopics.PO_BEFORE_NEW, MMovement.Table_Name);
		registerTableEvent(IEventTopics.PO_BEFORE_CHANGE, MMovement.Table_Name);
		registerTableEvent(IEventTopics.PO_BEFORE_NEW, MInvoiceLine.Table_Name);
		registerTableEvent(IEventTopics.PO_BEFORE_CHANGE,MInvoiceLine.Table_Name);
		registerTableEvent(IEventTopics.PO_AFTER_NEW,MInvoiceLine.Table_Name);
		registerTableEvent(IEventTopics.PO_BEFORE_NEW, MOrderLine.Table_Name);
		registerTableEvent(IEventTopics.PO_BEFORE_CHANGE, MOrderLine.Table_Name);
		registerTableEvent(IEventTopics.PO_AFTER_CHANGE,MOrderLine.Table_Name);
		registerTableEvent(IEventTopics.PO_AFTER_NEW,MInvoice.Table_Name);
		registerTableEvent(IEventTopics.PO_AFTER_CHANGE, MInvoice.Table_Name);		
		
		}

	@Override
	protected void doHandleEvent(Event event) {
		String type = event.getTopic();
		PO po = getPO(event);
		String msg = null;
		
		if (po.get_TableName().equals(MInvoice.Table_Name)
			&& type.equals(IEventTopics.PO_AFTER_CHANGE)){
			MInvoice invoice = ((MInvoice) po);
			  UpdatePS(invoice);
		}
		
		if (po.get_TableName().equals(MInvoice.Table_Name)
				&& type.equals(IEventTopics.PO_AFTER_NEW)){
			MInvoice invoice = ((MInvoice) po);
			UpdatePaymentMethodSRI(invoice);
			
		}
		
		if (po.get_TableName().equals(MInOut.Table_Name)
				&& type.equals(IEventTopics.PO_BEFORE_NEW)){
			MInOut inout = ((MInOut) po);
			   ShipDateUpdate(inout);
			
		}
		
		if (po.get_TableName().equals(MOrder.Table_Name)
				&& type.equals(IEventTopics.DOC_AFTER_COMPLETE)){
			MOrder order = ((MOrder) po);
			deletePaymentProgram(order);
		}
		

		if (po.get_TableName().equals(MInOut.Table_Name)
				&& type.equals(IEventTopics.DOC_BEFORE_COMPLETE)) {
			MInOut inout = ((MInOut) po);

			if(inout.getReversal_ID()>0)//CAMC ignorando inout con reversados
				return;
			
			validateInOut(inout);
		}

		if (po.get_TableName().equals(MInvoice.Table_Name)
				&& type.equals(IEventTopics.DOC_BEFORE_COMPLETE)) {
			MInvoice invoice = ((MInvoice) po);
			
			//log.log(Log.ERROR,"test: "+invoice.getDocumentNo()+" - totalline: "+invoice.getTotalLines());
			//log.warning("----------------------------------------------test: "+invoice.getDocumentNo()+" - totalline: "+invoice.getTotalLines());
			if(invoice.getTotalLines().compareTo(Env.ZERO)<=0 && invoice.getReversal_ID()>0)//CAMC ignorando facturas con valor negativo (reversadas)
				return;//CAMC ignorando facturas con valor negativo (reversadas)
			
			validateInvoice(invoice);
		}

		if (po.get_TableName().equals(MMovement.Table_Name)
				&& (type.equals(IEventTopics.DOC_BEFORE_COMPLETE) || type
						.equals(IEventTopics.PO_BEFORE_CHANGE))) {
			MMovement movement = ((MMovement) po);
			
			if(movement.getReversal_ID()>0)//CAMC ignorando movimientos con reversados
				return;

			// Hardcoded 1000418-SIS UIO COMPANIA RELACIONADA
			validateMovement(movement);
		}

		// before completing SO invoice set SO DocumentNo -- Previene custom
		// e-evolution Morder.completeIt
		if (po.get_TableName().equals(MInvoice.Table_Name)
				&& type.equals(IEventTopics.DOC_BEFORE_COMPLETE)) {
			MInvoice invoice = (MInvoice) po;
			if (MDocType.DOCSUBTYPESO_OnCreditOrder.equals(invoice.getC_Order()
					.getC_DocType().getDocSubTypeSO())) { // (W)illCall(I)nvoice
				invoice.setDocumentNo(invoice.getC_Order().getDocumentNo());
				invoice.saveEx();
			}

		}

		if(po.get_TableName().equals(MOrderLine.Table_Name)
			&& type.equals(IEventTopics.PO_AFTER_CHANGE)){
			MOrderLine oline = (MOrderLine) po;
			generateDiscount(oline);
			}

		// after completing SO invoice process electronic invoice
		if (po.get_TableName().equals(MInvoice.Table_Name)
				&& type.equals(IEventTopics.DOC_AFTER_COMPLETE)) {
			MInvoice invoice = (MInvoice) po;
			
			if(invoice.getTotalLines().compareTo(Env.ZERO)<=0 && invoice.getReversal_ID()>0)//CAMC ignorando facturas con valor negativo (reversadas)
				return;//CAMC ignorando facturas con valor negativo (reversadas)
			
			boolean IsGenerateInBatch = false;
			MDocType dt = new MDocType(invoice.getCtx(),
					invoice.getC_DocTypeTarget_ID(), invoice.get_TrxName());
			String shortdoctype = dt.get_ValueAsString("SRI_ShortDocType");

			if (invoice.getC_Order_ID() > 0) {
				MDocType dto = new MDocType(invoice.getCtx(), invoice
						.getC_Order().getC_DocType_ID(), invoice.get_TrxName());

				if (dto.get_ValueAsBoolean("IsGenerateInBatch"))
					IsGenerateInBatch = true;
			}

			if (dt.get_ValueAsBoolean("IsGenerateInBatch"))
				IsGenerateInBatch = true;

			//
			System.setProperty("javax.xml.soap.SAAJMetaFactory", "com.sun.xml.messaging.saaj.soap.SAAJMetaFactoryImpl");
			Boolean QSSLEC_FE_OFFLINE = true;//MSysConfig.getBooleanValue("QSSLEC_FE_OFFLINE", false, po.getAD_Client_ID(), po.getAD_Org_ID());
			//if (!shortdoctype.equals("") && !IsGenerateInBatch) {//CAMC
			if ((!shortdoctype.equals("") && !IsGenerateInBatch) || QSSLEC_FE_OFFLINE) {//CAMC

				msg = invoiceGenerateXml(invoice);
				if (msg != null)
					throw new AdempiereException(msg);
				Trx sriTrx = null;
				sriTrx = Trx.get(invoice.get_TrxName(), false);
				if (sriTrx != null) {
					sriTrx.commit();
				}
				invoice.load(sriTrx.getTrxName());
				String[] s = null;
				int t = 1;
				if(shortdoctype!=null && shortdoctype.length()>2){
					s = shortdoctype.split("-");
					t = s.length;
				}
				for(int i=0;i<t;i++){
					if(t>1)
						shortdoctype = s[i];
					if (invoice.get_Value(shortdoctype.equals("03")?"sri_authorization_invoice_ID":"SRI_Authorization_ID") != null) {
						int authorization = Integer.valueOf(invoice.get_Value(
								shortdoctype.equals("03")?"sri_authorization_invoice_ID":"SRI_Authorization_ID").toString());
						sendMail(authorization, null);
					}
				}
			}

		}

		// after completing SO inout process electronic inout
		if (po.get_TableName().equals(MInOut.Table_Name)
				&& type.equals(IEventTopics.DOC_AFTER_COMPLETE)) {
			MInOut inout = (MInOut) po;System.out.println(inout.getDocStatus());System.out.println(inout.getDocAction());
			boolean IsGenerateInBatch = false;
			MDocType dt = new MDocType(inout.getCtx(), inout.getC_DocType_ID(),
					inout.get_TrxName());
			String shortdoctype = dt.get_ValueAsString("SRI_ShortDocType");

			if (inout.getC_Order_ID() > 0) {
				MDocType dto = new MDocType(inout.getCtx(), inout.getC_Order()
						.getC_DocType_ID(), inout.get_TrxName());

				if (dto.get_ValueAsBoolean("IsGenerateInBatch"))
					IsGenerateInBatch = true;
			}

			if (dt.get_ValueAsBoolean("IsGenerateInBatch"))
				IsGenerateInBatch = true;

			//
			if (!shortdoctype.equals("") && !IsGenerateInBatch) {

				msg = inoutGenerateXml(inout);

				if (msg != null) {
					log.warning(msg);
					throw new RuntimeException(msg);
				}
				Trx sriTrx = null;
				sriTrx = Trx.get(inout.get_TrxName(), false);
				if (sriTrx != null) {
					sriTrx.commit();
				}
				inout.load(sriTrx.getTrxName());

				if (inout.get_Value("SRI_Authorization_ID") != null) {
					int authorization = Integer.valueOf(inout.get_Value(
							"SRI_Authorization_ID").toString());
					sendMail(authorization, null);
				}
			}
		}

		if (po.get_TableName().equals(MMovement.Table_Name)
				&& type.equals(IEventTopics.DOC_AFTER_COMPLETE)) {
			MMovement movement = (MMovement) po;

			MDocType dt = new MDocType(movement.getCtx(),
					movement.getC_DocType_ID(), movement.get_TrxName());
			String shortdoctype = dt.get_ValueAsString("SRI_ShortDocType");
			//
			if (!shortdoctype.equals("")
					&& !dt.get_ValueAsBoolean("IsGenerateInBatch")) {
				//
				msg = movementGenerateXml(movement);
				if (msg != null)
					throw new RuntimeException(msg);
				Trx sriTrx = null;
				sriTrx = Trx.get(movement.get_TrxName(), false);
				if (sriTrx != null) {
					sriTrx.commit();
				}
				movement.load(sriTrx.getTrxName());

				if (movement.get_Value("SRI_Authorization_ID") != null) {
					int authorization = Integer.valueOf(movement.get_Value(
							"SRI_Authorization_ID").toString());
					sendMail(authorization, null);
				}
			}
		}
		//Update Discount parameters  http://support.ingeint.com/issues/727
		if(po.get_TableName().equals(MInvoiceLine.Table_Name)
				&& (type.equals(IEventTopics.PO_AFTER_NEW))){

			MInvoiceLine invoiceline = (MInvoiceLine) po;

			if(invoiceline.getC_OrderLine()!=null){
			MOrderLine oline = new MOrderLine(invoiceline.getCtx(),invoiceline.getC_OrderLine_ID(),invoiceline.get_TrxName());
			if(oline.get_Value("DiscountAmt") !=null){
			invoiceline.set_ValueOfColumn("DiscountAmt", oline.get_Value("DiscountAmt"));
			invoiceline.saveEx();
			}
		  }
		}
		if (po.get_TableName().equals(MInvoiceLine.Table_Name)
				&& (type.equals(IEventTopics.PO_BEFORE_NEW) || type
						.equals(IEventTopics.PO_BEFORE_CHANGE))) {
			MInvoiceLine invoiceLine = (MInvoiceLine) po;
			MInvoice invoice = (MInvoice) invoiceLine.getC_Invoice();
			MDocType dt = new MDocType(invoice.getCtx(),
					invoice.getC_DocTypeTarget_ID(), invoice.get_TrxName());
			String shortdoctype = dt.get_ValueAsString("SRI_ShortDocType");
			//
			if (!shortdoctype.equals("")) {
				validateDigitAllowed(invoiceLine.getQtyEntered().doubleValue(),
						6, MInvoiceLine.COLUMNNAME_QtyEntered);// Max 6 digit on
																// factura_v1.1.0.xsd
																// SRI
				validateDigitAllowed(invoiceLine.getPriceEntered()
						.doubleValue(), 6, MInvoiceLine.COLUMNNAME_PriceEntered);// Max
																					// 2
																					// digit
																					// on
																					// factura_v1.1.0.xsd
																					// SRI
				validateDigitAllowed(invoiceLine.getLineNetAmt().doubleValue(),
						2, MInvoiceLine.COLUMNNAME_LineNetAmt);// Max 2 digit on
																// factura_v1.1.0.xsd
																// SRI
			}

		}

		if (po.get_TableName().equals(MOrderLine.Table_Name)
				&& (type.equals(IEventTopics.PO_BEFORE_NEW) || type
						.equals(IEventTopics.PO_BEFORE_CHANGE))) {
			MOrderLine orderLine = (MOrderLine) po;
			MOrder order = (MOrder) orderLine.getC_Order();
			MDocType dt = new MDocType(order.getCtx(),
					order.getC_DocTypeTarget_ID(), order.get_TrxName());
			int dtinvId = 0;
			if (dt.get_Value("C_DocTypeInvoice_ID") != null) {
				if (Integer.valueOf(dt.get_Value("C_DocTypeInvoice_ID")
						.toString()) > 0) {
					dtinvId = Integer.valueOf(dt.get_Value(
							"C_DocTypeInvoice_ID").toString());
					MDocType dtinv = new MDocType(order.getCtx(), dtinvId,
							order.get_TrxName());
					String shortdoctype = dtinv
							.get_ValueAsString("SRI_ShortDocType");
					//
					if (!shortdoctype.equals("")) {
						validateDigitAllowed(orderLine.getQtyEntered()
								.doubleValue(), 6,
								MOrderLine.COLUMNNAME_QtyEntered); // Max 6
																	// digit on
																	// factura_v1.1.0.xsd
						validateDigitAllowed(orderLine.getPriceEntered()
								.doubleValue(), 6,
								MOrderLine.COLUMNNAME_PriceEntered);// Max 2
																	// digit on
																	// factura_v1.1.0.xsd
						validateDigitAllowed(orderLine.getLineNetAmt()
								.doubleValue(), 2,
								MOrderLine.COLUMNNAME_LineNetAmt);// Max 2 digit
																	// on
																	// factura_v1.1.0.xsd
					}
				}
			}
		}

		if (po.get_TableName().equals(MLCOInvoiceWithholding.Table_Name)
				&& (type.equals(IEventTopics.PO_AFTER_NEW))) {
			MLCOInvoiceWithholding iwh = (MLCOInvoiceWithholding) po;
			MInvoice invoice = (MInvoice) iwh.getC_Invoice();
			if (!invoice.isSOTrx()) {
				boolean newWithholding = false;

				Object withholdingNo = invoice.get_Value("WithholdingNo");
				if (withholdingNo != null) {
					if (withholdingNo.toString().isEmpty()) {
						newWithholding = true;
					} else {
						newWithholding = false;
					}
				} else {
					newWithholding = true;
				}
				keepWithholdingDocumentNoAfterGenerated(iwh,invoice,
						newWithholding, invoice.get_TrxName());
			}

		}

		if (po.get_TableName().equals(MOrderLine.Table_Name)
				&& (type.equals(IEventTopics.PO_AFTER_CHANGE))){
			
		}
		
	}
	
	private void UpdatePaymentMethodSRI(MInvoice invoice) {
		
		if(invoice.getC_Order()!=null){
			
			MOrder order = new MOrder(invoice.getCtx(), invoice.getC_Order_ID(), invoice.get_TrxName());
			
			if(invoice.get_ValueAsString("LEC_PaymentMethod")==null || invoice.get_ValueAsString("LEC_PaymentMethod").length()==0)
				invoice.set_ValueOfColumn("LEC_PaymentMethod", order.get_Value("LEC_PaymentMethod"));
			invoice.saveEx();
			
		}
		
	}
	
	
	private void ShipDateUpdate(MInOut inout) {
		
		MOrder order = new MOrder(inout.getCtx(), inout.getC_Order_ID(), inout.get_TrxName());
		
		inout.set_ValueOfColumn("ShipDate", order.get_Value("ShipDate"));
		inout.set_ValueOfColumn("ShipDateE", order.get_Value("ShipDateE"));
	
	}
	
	
		private void UpdatePS(MInvoice invoice) {
			
			MInvoicePaySchedule[] ips = MInvoicePaySchedule.getInvoicePaySchedule(invoice.getCtx(), invoice.get_ID(), 0, invoice.get_TrxName());
			
			int rows = ips.length;

			for (int i = 0; i < rows; i++)

			{
				if (ips[i].get_Value("LEC_PaymentMethod") == null){
					ips[i].set_ValueOfColumn("LEC_PaymentMethod", invoice.get_Value("LEC_PaymentMethod"));
					ips[i].saveEx();
				}
			}
		
	}

	// http://support.ingeint.com/issues/774
	
	private void deletePaymentProgram(MOrder order) {
		
		MOrderPaySchedule[] ops = MOrderPaySchedule.getOrderPaySchedule(order.getCtx(),
						order.get_ID(), 0, order.get_TrxName());
		
		int rows = ops.length;
		
		for (int i = 0; i < rows; i++)

		{
			ops[i].deleteEx(true);
		}
		order.setIsPayScheduleValid(false);
	}


	//  www.ingeint.com - http://support.ingeint.com/issues/727
	//AfterChange
	
	private void generateDiscount(MOrderLine oline) {

		if(false){//CM correccion de bug // arreglar 
		//if(oline.get_ValueAsBoolean("IsDiscountApplied")){
			BigDecimal calcdesc = DB.getSQLValueBD(oline.get_TrxName(),"SELECT sum(DiscountAmt) FROM C_OrderLine WHERE C_Order_ID = ? ",oline.getC_Order_ID());

			MOrder order = (MOrder) oline.getC_Order();
			int res = calcdesc.compareTo(order.getTotalLines());
			
				if(res==1){
					throw new AdempiereException("El Descuento no puede ser mayor al total de las lineas de la orden");
				}
				BigDecimal discount = (BigDecimal) oline.get_Value("DiscountAmt");
				if(discount !=null){
					Integer LineOrder = DB.getSQLValue(oline.get_TrxName(), "SELECT C_OrderLine_ID FROM C_OrderLine WHERE C_Order_ID = ? AND C_Charge_ID IN"
							+ " (Select C_Charge_ID FROM C_Charge WHERE isDiscountFE='Y') ", oline.getC_Order_ID());

					if(LineOrder >0){

						MOrderLine chargeline = new MOrderLine(oline.getCtx(),LineOrder,oline.get_TrxName());
						if(discount.compareTo(BigDecimal.ZERO) == 0){
							chargeline.delete(true);
						}else{
							chargeline.setPriceEntered(calcdesc.negate());
							chargeline.setPriceActual(calcdesc.negate());
							chargeline.setLineNetAmt(calcdesc.negate());
							chargeline.saveEx();
							log.warning("-----------------Aqui Actualizo el Cargo");
						}
					}
					else
					{
						MOrderLine chargeline = new MOrderLine(order);

						Integer C_ChargeID = DB.getSQLValue(null, " SELECT C_Charge_ID FROM C_Charge WHERE IsDiscountFE='Y' ");
						if (C_ChargeID <=0)
							throw new AdempiereException("Debe configurar un cargo para descuentos de Facturación Electrónica");

						chargeline.setC_Charge_ID(C_ChargeID);
						chargeline.setQtyOrdered(Env.ONE);
						chargeline.setQtyEntered(BigDecimal.ONE);
						chargeline.setPriceEntered(calcdesc.negate());
						chargeline.setPriceActual(calcdesc.negate());
						chargeline.setLineNetAmt(calcdesc.negate());
						chargeline.saveEx();
						log.warning("-----------------Aqui Creo el Nuevo Cargo");
					}
				}
			
			oline.set_ValueOfColumn("IsDiscountApplied", "N");
			log.warning("----Se Ejecutó el descuento");
			oline.saveEx();
		}
	}
	
	
	public static String invoiceGenerateXml(MInvoice inv) {
		int autorization_id = 0;
		String msg = null;
		MDocType dt = new MDocType(inv.getCtx(), inv.getC_DocTypeTarget_ID(),
				inv.get_TrxName());
		
		

		String shortdoctype = dt.get_ValueAsString("SRI_ShortDocType");
		
		if (shortdoctype.equals("")) {
			msg = "No existe definicion SRI_ShortDocType: " + dt.toString();
			log.info("Invoice: " + inv.toString() + msg);

			// if (LEC_FE_Utils.breakDialog(msg)) return "Cancelado..."; // Temp

		}

		MUser user = new MUser(inv.getCtx(), inv.getAD_User_ID(),
				inv.get_TrxName());
		
		msg = null;
		LEC_FE_MInvoice lecfeinv = new LEC_FE_MInvoice(inv.getCtx(),
				inv.getC_Invoice_ID(), inv.get_TrxName());
		LEC_FE_MLiquidacionCompras lecfeinvlc = new LEC_FE_MLiquidacionCompras(inv.getCtx(),
				inv.getC_Invoice_ID(), inv.get_TrxName());
		LEC_FE_MNotaCredito lecfeinvnc = new LEC_FE_MNotaCredito(inv.getCtx(),
				inv.getC_Invoice_ID(), inv.get_TrxName());
		LEC_FE_MNotaDebito lecfeinvnd = new LEC_FE_MNotaDebito(inv.getCtx(),
				inv.getC_Invoice_ID(), inv.get_TrxName());
		LEC_FE_MRetencion lecfeinvret = new LEC_FE_MRetencion(inv.getCtx(),
				inv.getC_Invoice_ID(), inv.get_TrxName());
		// isSOTrx()
		if (inv.isSOTrx())
			LEC_FE_MRetencion.generateWitholdingNo(inv);
		
		String[] s = null;
		int t = 1;
		if(shortdoctype!=null && shortdoctype.length()>2){
			s = shortdoctype.split("-");
			t = s.length;
		}
		for(int i=0;i<t;i++){
			if(t>1)
				shortdoctype = s[i];
			if (inv.get_Value(shortdoctype.equals("03")?"sri_authorization_invoice_ID":"SRI_Authorization_ID") != null) {
				autorization_id = inv.get_ValueAsInt(shortdoctype.equals("03")?"sri_authorization_invoice_ID":"SRI_Authorization_ID");
			}
		
		
			if (autorization_id != 0) {
				X_SRI_Authorization a = new X_SRI_Authorization(inv.getCtx(),
						autorization_id, inv.get_TrxName());
				if (a != null) {
					if (a.getSRI_AuthorizationDate() != null && !a.getSRI_AuthorizationCode().equalsIgnoreCase("0")) {
						// Comprobante autorizado, no se envia de nuevo el xml.
						continue;
					}
				}
			}
			
			
	
			/*			//CAMC //ya no es requerido que el usuario tenga email
			if (!valideUserMail(user) && !shortdoctype.equals("")) {
				msg = "@RequestActionEMailNoTo@";
				return msg;
			}
			*/
	
			if (shortdoctype.equals("01")) { // FACTURA
				msg = lecfeinv.lecfeinv_SriExportInvoiceXML100();
			} else if (shortdoctype.startsWith("03")) { // LIQUIDACI�N DE COMPRAS
				msg = lecfeinvlc.lecfeinv_SriExportLiquidacionCompraXML100();	
			} else if (shortdoctype.equals("04")) { // NOTA DE CRÉDITO
				msg = lecfeinvnc.lecfeinvnc_SriExportNotaCreditoXML100();
			} else if (shortdoctype.equals("05")) { // NOTA DE DÉBITO
				msg = lecfeinvnd.lecfeinvnd_SriExportNotaDebitoXML100();
				// !isSOTrx()
			} else if (shortdoctype.equals("07")) { // COMPROBANTE DE RETENCIÓN
				int idAutRet = lecfeinvret.get_ValueAsInt("SRI_Authorization_ID");
				if (idAutRet < 1 || new X_SRI_Authorization(Env.getCtx(), idAutRet, inv.get_TrxName()).getSRI_AuthorizationCode().toString().equalsIgnoreCase("0")
						&& MSysConfig.getBooleanValue(
								"LEC_GenerateWitholdingToComplete", false,
								lecfeinvret.getAD_Client_ID(), lecfeinvret.getAD_Org_ID())) {
					//LEC_FE_MRetencion.generateWitholdingNo(inv);//CAMC no volver a generar las retenciones al completar la factura(se deben generar con el boton generar retenciones)
					//Trx tra = Trx.get(inv.get_TrxName(), false);
					//tra.commit();
					if(t>1 && msg!=null){
						msg += lecfeinvret.lecfeinvret_SriExportRetencionXML100();
					}else
						msg = lecfeinvret.lecfeinvret_SriExportRetencionXML100();
					
				}
			} else
				log.warning("Formato no habilitado SRI: " + dt.toString()
						+ shortdoctype);
		}

		return msg;
	}

	private String inoutGenerateXml(MInOut inout) {
		int autorization_id = 0;
		if (inout.get_Value("SRI_Authorization_ID") != null) {
			autorization_id = inout.get_ValueAsInt("SRI_Authorization_ID");
		}
		X_SRI_Authorization a = new X_SRI_Authorization(inout.getCtx(),
				autorization_id, inout.get_TrxName());
		if (a != null) {
			if (a.getSRI_AuthorizationDate() != null) {
				// Comprobante autorizado, no se envia de nuevo el xml.
				return null;
			}
		}
		String msg = null;

		MDocType dt = new MDocType(inout.getCtx(), inout.getC_DocType_ID(),
				inout.get_TrxName());

		String shortdoctype = dt.get_ValueAsString("SRI_ShortDocType");

		if (shortdoctype.equals("")) {
			msg = "No existe definicion SRI_ShortDocType: " + dt.toString();
			log.info("Invoice: " + inout.toString() + msg);

			// if (LEC_FE_Utils.breakDialog(msg)) return "Cancelado..."; // Temp
		}

		MUser user = new MUser(inout.getCtx(), inout.getAD_User_ID(),
				inout.get_TrxName());

		if (!valideUserMail(user) && !shortdoctype.equals("")) {
			msg = "@RequestActionEMailNoTo@";
			return msg;
		}

		msg = null;
		LEC_FE_MInOut lecfeinout = new LEC_FE_MInOut(inout.getCtx(),
				inout.getM_InOut_ID(), inout.get_TrxName());
		// isSOTrx()
		if (shortdoctype.equals("06")) // GU�A DE REMISI�N
			msg = lecfeinout.lecfeinout_SriExportInOutXML100();
		else
			log.warning("Formato no habilitado SRI: " + dt.toString()
					+ shortdoctype);

		return msg;
	}

	private String movementGenerateXml(MMovement movement) {
		int autorization_id = 0;
		if (movement.get_Value("SRI_Authorization_ID") != null) {
			autorization_id = movement.get_ValueAsInt("SRI_Authorization_ID");
		}
		X_SRI_Authorization a = new X_SRI_Authorization(movement.getCtx(),
				autorization_id, movement.get_TrxName());
		if (a != null) {
			if (a.getSRI_AuthorizationDate() != null) {
				// Comprobante autorizado, no se envia de nuevo el xml.
				return null;
			}
		}
		String msg = null;

		MDocType dt = new MDocType(movement.getCtx(),
				movement.getC_DocType_ID(), movement.get_TrxName());

		String shortdoctype = dt.get_ValueAsString("SRI_ShortDocType");

		if (shortdoctype.equals("")) {
			msg = "No existe definicion SRI_ShortDocType: " + dt.toString();
			log.info("Invoice: " + movement.toString() + msg);

			// if (LEC_FE_Utils.breakDialog(msg)) return "Cancelado..."; // Temp
		}

		MUser user = new MUser(movement.getCtx(), movement.getAD_User_ID(),
				movement.get_TrxName());

		if (!valideUserMail(user) && !shortdoctype.equals("")) {
			msg = "@RequestActionEMailNoTo@";
			return msg;
		}

		msg = null;
		LEC_FE_Movement lecfemovement = new LEC_FE_Movement(movement.getCtx(),
				movement.getM_Movement_ID(), movement.get_TrxName());
		// Hardcoded 1000418-SIS UIO COMPANIA RELACIONADA
		// if (shortdoctype.equals("06") && dt.getC_DocType_ID() == 1000418) //
		// GU�A DE REMISI�N
		if (shortdoctype.equals("06"))
			msg = lecfemovement.lecfeMovement_SriExportMovementXML100();
		else
			log.warning("Formato no habilitado SRI: " + dt.toString()
					+ shortdoctype);

		return msg;
	}

	public static boolean valideUserMail(MUser user) {
		if(user==null || user.get_ID()<=0)//camc
			return true;//Camc
		if (MSysConfig.getBooleanValue("QSSLEC_FE_EnvioXmlAutorizadoBPEmail",
				false, user.getAD_Client_ID(), user.getAD_Org_ID())) {

			if ((user.get_ID() == 0 || user.isNotificationEMail()
					&& (user.getEMail() == null || user.getEMail().length() == 0))) {
				return false;
			}
		}

		return true;

	} // valideUserMail

	public static void sendMail(int p_authorization, Trx trx) {
		MProcess process = new Query(Env.getCtx(), MProcess.Table_Name,
				"classname = ?", null).setParameters(
				"org.globalqss.process.SRIEmailAuthorization").first();
		if (process != null) {

			ProcessInfo processInfo = new ProcessInfo(process.getName(),
					process.get_ID());
			MPInstance instance = new MPInstance(Env.getCtx(),
					processInfo.getAD_Process_ID(), processInfo.getRecord_ID());
			instance.save();

			ProcessInfoParameter[] para = { new ProcessInfoParameter(
					"SRI_Authorization_ID", p_authorization, null, null, null) };
			processInfo.setAD_Process_ID(process.get_ID());
			processInfo.setClassName(process.getClassname());
			processInfo.setAD_PInstance_ID(instance.getAD_PInstance_ID());
			processInfo.setParameter(para);

			ProcessUtil.startJavaProcess(Env.getCtx(), processInfo, trx, true);
		}
	}

	/**
	 * valideOrgInfoSri
	 *
	 * @param MOrgInfo
	 *            orginfo
	 * @return error message or null
	 */
	public static String valideOrgInfoSri(MOrgInfo orginfo) {

		String msg = null;

		int c_location_matriz_id = MSysConfig.getIntValue(
				"QSSLEC_FE_LocalizacionDireccionMatriz", -1,
				//orginfo.getAD_Client_ID());
				orginfo.getAD_Client_ID(),orginfo.getAD_Org_ID());//CAMBIO CAMC!!!! para que detecte diferentes localizaciones dependiendo de la organizacion

		if (c_location_matriz_id < 1)
			msg = "No existe parametro para LocalizacionDireccionMatriz";

		String clavecert = MSysConfig.getValue(
				"QSSLEC_FE_ClaveCertificadoDigital", null,
				orginfo.getAD_Client_ID(), orginfo.getAD_Org_ID());

		if (clavecert == null)
			msg = "No existe parametro para ClaveCertificadoDigital";

		String rutacert = MSysConfig.getValue(
				"QSSLEC_FE_RutaCertificadoDigital", null,
				orginfo.getAD_Client_ID(), orginfo.getAD_Org_ID());
		// msg = "No existe parametro para RutaCertificadoDigital";

		if (rutacert != null) {
			File folder = new File(rutacert);

			if (!folder.exists() || !folder.isFile() || !folder.canRead())
				msg = "No existe o no se puede leer el archivo de Certificado Digital";

		} else {
			// Obtencion del certificado para firmar. Utilizando un attachment -
			// AD_Org
			boolean isattachcert = false;
			MAttachment attach = MAttachment.get(orginfo.getCtx(),
					MTable.getTable_ID("AD_Org"), orginfo.getAD_Org_ID());
			if (attach != null) {
				for (MAttachmentEntry entry : attach.getEntries()) {
					if (entry.getName().endsWith("p12")
							|| entry.getName().endsWith("pfx"))
						isattachcert = true;
				}
			}
			if (!isattachcert)
				msg = "No existe parametro o adjunto de Certificado Digital";
		}

		int c_bpartner_id = LEC_FE_Utils.getOrgBPartner(
				orginfo.getAD_Client_ID(), orginfo.get_ValueAsString("TaxID"));

		if (c_bpartner_id < 1)
			msg = "No existe BP relacionado a OrgInfo.Documento: "
					+ orginfo.get_ValueAsString("TaxID");
		else if (orginfo.get_ValueAsString("TaxID").equals(""))
			msg = "No existe definicion OrgInfo.Documento: "
					+ orginfo.toString();
		else if (orginfo.get_ValueAsString("SRI_DocumentCode").equals(""))
			msg = "No existe definicion OrgInfo.DocumentCode: "
					+ orginfo.toString();
		/*else if (orginfo.get_ValueAsString("SRI_OrgCode").equals(""))//CAMC ya no valido el campo SRI_OrgCode
			msg = "No existe definicion OrgInfo.SRI_OrgCode: "
					+ orginfo.toString();*/
		// else if (orginfo.get_ValueAsString("SRI_StoreCode").equals(""))
		// msg = "No existe definicion OrgInfo.SRI_StoreCode: " +
		// orginfo.toString();
		else if (orginfo.get_ValueAsString("SRI_DocumentCode").equals(""))
			msg = "No existe definicion OrgInfo.SRI_DocumentCode: "
					+ orginfo.toString();
		else if (orginfo.get_ValueAsString("SRI_IsKeepAccounting").equals(""))
			msg = "No existe definicion OrgInfo.SRI_IsKeepAccounting: "
					+ orginfo.toString();
		else if (orginfo.getC_Location_ID() == 0)
			msg = "No existe definicion OrgInfo.Address1: "
					+ orginfo.toString();
		/*else {
			MBPartner bpe = new MBPartner(orginfo.getCtx(), c_bpartner_id,
					orginfo.get_TrxName());
			if ((Integer) bpe.get_Value("LCO_TaxPayerType_ID") == 1000027) // Hardcoded
				if (orginfo.get_ValueAsString("SRI_TaxPayerCode").equals(""))
					msg = "No existe definicion OrgInfo.SRI_TaxPayerCode: "
							+ orginfo.toString();
			;
		}*/

		return msg;

	} // valideOrgInfoSri

	/**
	 * valideInvoice
	 *
	 * @param MInvoice
	 *
	 * @return error message or null
	 */

	private void validateInvoice(MInvoice invoice) {
		MDocType dt = new MDocType(invoice.getCtx(),
				invoice.getC_DocTypeTarget_ID(), invoice.get_TrxName());
		if (MSysConfig.getBooleanValue(
				"QSSLEC_FE_UseClaveContingenciaVentaMostradoryFolio", false,
				invoice.getAD_Client_ID(),invoice.getAD_Org_ID())
				&& invoice.isSOTrx()
				&& invoice.getC_Order_ID() > 0
				&& MDocType.DOCSUBTYPESO_POSOrder.equals(invoice.getC_Order()
						.getC_DocType().getDocSubTypeSO()) // (W)illCall(I)nvoice
				&& dt.get_Value("SRI_ShortDocType") != null) {
			invoice.set_ValueOfColumn("SRI_IsUseContingency", "Y");
		}
		if (dt.get_Value("SRI_ShortDocType") != null) {
			/*if (invoice.getAD_User_ID() <= 0)//CAMC
				throw new AdempiereException(Msg.translate(Env.getCtx(),
						"FillMandatory")
						+ " "
						+ Msg.getElement(Env.getCtx(),
								MInvoice.COLUMNNAME_AD_User_ID));*/
			MBPartner bp = MBPartner.get(Env.getCtx(),
					invoice.getC_BPartner_ID());
			//CAMC //ya no es requerido que el usuario tenga email
			/*if (!valideUserMail((MUser) invoice.getAD_User()))
				throw new AdempiereException(Msg.translate(Env.getCtx(),
						"FillMandatory")
						+ " "
						+ Msg.getElement(Env.getCtx(),
								MUser.COLUMNNAME_AD_User_ID)
						+ " - "
						+ Msg.getElement(Env.getCtx(), MUser.COLUMNNAME_EMail));*/
			if (bp.get_Value(X_LCO_TaxIdType.COLUMNNAME_LCO_TaxIdType_ID) == null)
				throw new AdempiereException(Msg.translate(Env.getCtx(),
						"FillMandatory")
						+ " "
						+ Msg.getElement(Env.getCtx(),
								MBPartner.COLUMNNAME_C_BPartner_ID)
						+ " - "
						+ Msg.getElement(Env.getCtx(),
								X_LCO_TaxIdType.COLUMNNAME_LCO_TaxIdType_ID));
			if (bp.get_Value(X_LCO_TaxPayerType.COLUMNNAME_LCO_TaxPayerType_ID) == null)
				throw new AdempiereException(
						Msg.translate(Env.getCtx(), "FillMandatory")
								+ " "
								+ Msg.getElement(Env.getCtx(),
										MBPartner.COLUMNNAME_C_BPartner_ID)
								+ " - "
								+ Msg.getElement(
										Env.getCtx(),
										X_LCO_TaxPayerType.COLUMNNAME_LCO_TaxPayerType_ID));
			if (dt.get_ValueAsString("SRI_ShortDocType").equals("05")
					|| dt.get_ValueAsString("SRI_ShortDocType").equals("04")) {
				if (invoice.get_Value("SRI_RefInvoice_ID") == null)
					throw new AdempiereException(Msg.translate(Env.getCtx(),
							"FillMandatory")
							+ " "
							+ Msg.getElement(Env.getCtx(), "SRI_RefInvoice_ID"));
				if (invoice.getDescription() == null)
					throw new AdempiereException(Msg.translate(Env.getCtx(),
							"FillMandatory")
							+ " "
							+ Msg.getElement(Env.getCtx(),
									MInvoice.COLUMNNAME_Description));
			}
			if (dt.get_ValueAsString("SRI_ShortDocType").equals("07")) {
				if (!invoice.get_ValueAsBoolean("SRI_IsUseContingency")){
					//System.out.println("id de la autorizacion: "+invoice.get_Value(X_SRI_Authorization.COLUMNNAME_SRI_Authorization_ID));
					//System.out.println("id de la autorizacion: "+invoice.get_ID());
					//System.out.println("id de la autorizacion: "+invoice.getDocumentNo());
					if (invoice
							.get_Value(X_SRI_Authorization.COLUMNNAME_SRI_Authorization_ID) == null)
						throw new AdempiereException(
								Msg.translate(Env.getCtx(), "FillMandatory")
										+ " "
										+ Msg.getElement(
												Env.getCtx(),
												X_SRI_Authorization.COLUMNNAME_SRI_Authorization_ID));
				}
			}
			if (dt.get_ValueAsString("SRI_ShortDocType").equals("03")) {
				if (!invoice.get_ValueAsBoolean("SRI_IsUseContingency")){
					//System.out.println("id de la autorizacion: "+invoice.get_Value(X_SRI_Authorization.COLUMNNAME_SRI_Authorization_ID));
					//System.out.println("id de la autorizacion: "+invoice.get_ID());
					//System.out.println("id de la autorizacion: "+invoice.getDocumentNo());
					if (invoice
							.get_Value("sri_authorization_invoice_ID") == null)
						throw new AdempiereException(
								Msg.translate(Env.getCtx(), "FillMandatory")
										+ " "
										+ Msg.getElement(
												Env.getCtx(),
												"sri_authorization_invoice_ID"));
				}
			}
			if (dt.get_ValueAsString("SRI_ShortDocType").equals("03-07")) {
				if (!invoice.get_ValueAsBoolean("SRI_IsUseContingency")){
					//System.out.println("id de la autorizacion: "+invoice.get_Value(X_SRI_Authorization.COLUMNNAME_SRI_Authorization_ID));
					//System.out.println("id de la autorizacion: "+invoice.get_ID());
					//System.out.println("id de la autorizacion: "+invoice.getDocumentNo());
					if (invoice
							.get_Value("sri_authorization_invoice_ID") == null || invoice
							.get_Value(X_SRI_Authorization.COLUMNNAME_SRI_Authorization_ID) == null)
						throw new AdempiereException(
								Msg.translate(Env.getCtx(), "FillMandatory")
										+ " "
										+ Msg.getElement(
												Env.getCtx(),
												"sri_authorization_invoice_ID") + " , " + Msg.getElement(
														Env.getCtx(),
														X_SRI_Authorization.COLUMNNAME_SRI_Authorization_ID));
				}
			}
			if (dt.get_Value("IsInternal") != null)
				if (dt.get_ValueAsBoolean("IsInternal")) {
					if (invoice.get_Value("et_incoterms") == null)
						throw new AdempiereException(Msg.translate(
								Env.getCtx(), "FillMandatory")
								+ " "
								+ Msg.getElement(Env.getCtx(), "et_incoterms"));
					if (invoice.get_Value("SRI_ComercioExterior") == null)
						throw new AdempiereException(Msg.translate(
								Env.getCtx(), "FillMandatory")
								+ " "
								+ Msg.getElement(Env.getCtx(),
										"SRI_ComercioExterior"));
					boolean address = false;
					if (invoice.getC_Order_ID() > 0) {
						if (invoice.getC_Order().getC_BPartner_Location() != null) {
							if (invoice.getC_Order().getC_BPartner_Location()
									.getC_Location() != null)
								if (invoice.getC_Order()
										.getC_BPartner_Location()
										.getC_Location().getAddress1() != null)
									address = true;
						}
					}
					if (!address)
						throw new AdempiereException(Msg.translate(
								Env.getCtx(), "FillMandatory")
								+ " "
								+ Msg.getElement(Env.getCtx(),
										MOrder.COLUMNNAME_C_Order_ID)
								+ " - "
								+ Msg.getElement(Env.getCtx(),
										MLocation.COLUMNNAME_Address1));
				}
		}
		if (!invoice.getPaymentRule().equals(invoice.PAYMENTRULE_OnCredit)){
			MInvoicePaySchedule[] ips = MInvoicePaySchedule.getInvoicePaySchedule(invoice.getCtx(),
					invoice.get_ID(), 0, invoice.get_TrxName());

			int rows = ips.length;

			for (int i = 0; i < rows; i++){
				ips[i].deleteEx(true);
			}
			invoice.setIsPayScheduleValid(false);
		}

	}// validateInovice

	/**
	 * valideMInOut
	 *
	 * @param MInOut
	 *
	 * @return error message or null
	 */

	private void validateInOut(MInOut inout) {
		MDocType dt = new MDocType(inout.getCtx(), inout.getC_DocType_ID(),
				inout.get_TrxName());
		if (MSysConfig.getBooleanValue(
				"QSSLEC_FE_UseClaveContingenciaVentaMostradoryFolio", false,
				inout.getAD_Client_ID(), inout.getAD_Org_ID())
				&& inout.isSOTrx()
				&& inout.getC_Order_ID() > 0
				&& MDocType.DOCSUBTYPESO_POSOrder.equals(inout.getC_Order()
						.getC_DocType().getDocSubTypeSO()) // (W)illCall(I)nvoice
				&& dt.get_Value("SRI_ShortDocType") != null) {
			inout.set_ValueOfColumn("SRI_IsUseContingency", "Y");
		}
		if (dt.get_Value("SRI_ShortDocType") != null) {
			if (inout.getAD_User_ID() <= 0)
				throw new AdempiereException(Msg.translate(Env.getCtx(),
						"FillMandatory")
						+ " "
						+ Msg.getElement(Env.getCtx(),
								MInvoice.COLUMNNAME_AD_User_ID));
			if (!valideUserMail((MUser) inout.getAD_User()))
				throw new AdempiereException(Msg.translate(Env.getCtx(),
						"FillMandatory")
						+ " "
						+ Msg.getElement(Env.getCtx(),
								MUser.COLUMNNAME_AD_User_ID)
						+ " - "
						+ Msg.getElement(Env.getCtx(), MUser.COLUMNNAME_EMail));
			MBPartner bp = MBPartner
					.get(Env.getCtx(), inout.getC_BPartner_ID());
			if (bp.get_Value(X_LCO_TaxIdType.COLUMNNAME_LCO_TaxIdType_ID) == null)
				throw new AdempiereException(Msg.translate(Env.getCtx(),
						"FillMandatory")
						+ " "
						+ Msg.getElement(Env.getCtx(),
								MBPartner.COLUMNNAME_C_BPartner_ID)
						+ " - "
						+ Msg.getElement(Env.getCtx(),
								X_LCO_TaxIdType.COLUMNNAME_LCO_TaxIdType_ID));
			if (!inout.getDeliveryViaRule().equals(
					MInOut.DELIVERYVIARULE_Shipper))
				throw new AdempiereException(Msg.translate(Env.getCtx(),
						"FillMandatory")
						+ " "
						+ Msg.getElement(Env.getCtx(),
								MInOut.COLUMNNAME_DeliveryViaRule));
			if (inout.getM_Shipper() == null)
				throw new AdempiereException(Msg.translate(Env.getCtx(),
						"FillMandatory")
						+ " "
						+ Msg.getElement(Env.getCtx(),
								MInOut.COLUMNNAME_M_Shipper_ID));
			if (inout.getShipDate() == null)
				throw new AdempiereException(Msg.translate(Env.getCtx(),
						"FillMandatory")
						+ " "
						+ Msg.getElement(Env.getCtx(),
								MInOut.COLUMNNAME_ShipDate));
			if (inout.get_Value("ShipDateE") == null)
				throw new AdempiereException(Msg.translate(Env.getCtx(),
						"FillMandatory")
						+ " "
						+ Msg.getElement(Env.getCtx(), "ShipDateE"));
			if (inout.getC_BPartner_Location() == null) {
				if (inout.getC_BPartner_Location_ID() <= 0)
					throw new AdempiereException(Msg.translate(Env.getCtx(),
							"FillMandatory")
							+ " "
							+ Msg.getElement(Env.getCtx(),
									MInOut.COLUMNNAME_C_BPartner_Location_ID));
			}
		}
	}// validateInOut

	/**
	 * valideMInOut
	 *
	 * @param MInOut
	 *
	 * @return error message or null
	 */
	private void validateMovement(MMovement movement) {
		String msg = "";
		MDocType dt = new MDocType(movement.getCtx(),
				movement.getC_DocType_ID(), movement.get_TrxName());
		String shortdoctype = dt.get_ValueAsString("SRI_ShortDocType");
		if (shortdoctype.equals("06")
				&& (movement.getDescription() == null || movement
						.getDescription().trim().length() == 0)) { // GU�A DE
																	// REMISI�N
			msg = "Descripcion obligatoria para el comprobante electronico";
			throw new AdempiereException(msg);
		}
		if (dt.get_Value("SRI_ShortDocType") != null) {
			if (movement.getAD_User_ID() <= 0)
				throw new AdempiereException(Msg.translate(Env.getCtx(),
						"FillMandatory")
						+ " "
						+ Msg.getElement(Env.getCtx(),
								MMovement.COLUMNNAME_AD_User_ID));
			if (!valideUserMail((MUser) movement.getAD_User()))
				throw new AdempiereException(Msg.translate(Env.getCtx(),
						"FillMandatory")
						+ " "
						+ Msg.getElement(Env.getCtx(),
								MUser.COLUMNNAME_AD_User_ID)
						+ " - "
						+ Msg.getElement(Env.getCtx(), MUser.COLUMNNAME_EMail));
			MBPartner bp = MBPartner.get(Env.getCtx(),
					movement.getC_BPartner_ID());
			if (bp.get_Value(X_LCO_TaxIdType.COLUMNNAME_LCO_TaxIdType_ID) == null)
				throw new AdempiereException(Msg.translate(Env.getCtx(),
						"FillMandatory")
						+ " "
						+ Msg.getElement(Env.getCtx(),
								MBPartner.COLUMNNAME_C_BPartner_ID)
						+ " - "
						+ Msg.getElement(Env.getCtx(),
								X_LCO_TaxIdType.COLUMNNAME_LCO_TaxIdType_ID));
			if (!movement.getDeliveryViaRule().equals(
					MInOut.DELIVERYVIARULE_Shipper))
				throw new AdempiereException(Msg.translate(Env.getCtx(),
						"FillMandatory")
						+ " "
						+ Msg.getElement(Env.getCtx(),
								MMovement.COLUMNNAME_DeliveryViaRule));
			if (movement.getM_Shipper() == null)
				throw new AdempiereException(Msg.translate(Env.getCtx(),
						"FillMandatory")
						+ " "
						+ Msg.getElement(Env.getCtx(),
								MMovement.COLUMNNAME_M_Shipper_ID));
			if (movement.get_Value("ShipDate") == null)
				throw new AdempiereException(Msg.translate(Env.getCtx(),
						"FillMandatory")
						+ " "
						+ Msg.getElement(Env.getCtx(), "ShipDate"));
			if (movement.get_Value("ShipDateE") == null)
				throw new AdempiereException(Msg.translate(Env.getCtx(),
						"FillMandatory")
						+ " "
						+ Msg.getElement(Env.getCtx(), "ShipDateE"));
			if (movement.getC_BPartner_Location() == null) {
				if (movement.getC_BPartner_Location_ID() <= 0)
					throw new AdempiereException(
							Msg.translate(Env.getCtx(), "FillMandatory")
									+ " "
									+ Msg.getElement(
											Env.getCtx(),
											MMovement.COLUMNNAME_C_BPartner_Location_ID));
			}
		}
	}// validateMovement

	/**
	 * valide DigitAllowed
	 *
	 * @param qtyEntered
	 *
	 * @return void or throw Maximum allowed two decimal digits
	 */
	private void validateDigitAllowed(double amt, int digitAllowed,
			String ColumnName) {
		String amtStr = String.valueOf(amt);
		int pos = 0;
		String decimal = "";
		pos = amtStr.lastIndexOf('.');

		if (pos > 0) {
			decimal = amtStr.substring(pos);
		}
		if ((decimal.length() - 1) > digitAllowed) { // .00 = 3 Digits
			throw new AdempiereException(Msg.translate(Env.getCtx(),
					//"Maximum allowed two decimal digits")
					"Maximum allowed "+digitAllowed+" decimal digits")//CAMC
					+ " - "
					+ Msg.getElement(Env.getCtx(), ColumnName));
		}
	}// validateMovement

//	/**
//	 * keepWithholdingDocumentNoAfterGenerated
//	 *
//	 * @param MInvoice
//	 *            invoice, String trxName
//	 *
//	 * @return void
//	 */
//	private String keepWithholdingDocumentNoAfterGenerated(MInvoice invoice,
//			boolean newWithholding, String trxName) {
//		String withhodingNo = "";
//		boolean keep = MSysConfig.getBooleanValue(
//				"QSSLEC_FE_KEEP_WITHHOLDING_DOCNO_AFTER_GENERATED", false,
//				Env.getAD_Client_ID(Env.getCtx()),Env.getAD_Org_ID(Env.getCtx()));
//		if (!keep)
//			return null;
//		log.log(Log.INFO,
//				"QSSLEC_FE_KEEP_WITHHOLDING_DOCNO_AFTER_GENERATED = Y");
//		if (newWithholding) {
//			withhodingNo = LEC_FE_MRetencion.generateWitholdingNo(invoice);
//			invoice.set_ValueOfColumn("WithholdingNo", withhodingNo);
//			invoice.saveEx();
//		} else {
//			withhodingNo = invoice.get_ValueAsString("WithholdingNo");
//			List<MLCOInvoiceWithholding> withholdings = new Query(
//					invoice.getCtx(), MLCOInvoiceWithholding.Table_Name,
//					"C_Invoice_ID=?", invoice.get_TrxName())
//					.setParameters(invoice.getC_Invoice_ID())
//					.setOnlyActiveRecords(true).list();
//			for (MLCOInvoiceWithholding wth : withholdings) {
//				wth.setDocumentNo(withhodingNo);
//				wth.saveEx();
//			}
//		}
//		return withhodingNo;
//	}

	/**
	 * keepWithholdingDocumentNoAfterGenerated
	 *
	 * @param MLCOInvoiceWithholding iwh,MInvoice invoice,
			boolean newWithholding, String trxName
	 *
	 * @return String withhodingNo
	 */
	private String keepWithholdingDocumentNoAfterGenerated(MLCOInvoiceWithholding iwh,MInvoice invoice,
			boolean newWithholding, String trxName) {
		String withhodingNo = "";
		boolean keep = MSysConfig.getBooleanValue(
				"QSSLEC_FE_KEEP_WITHHOLDING_DOCNO_AFTER_GENERATED", false,
				Env.getAD_Client_ID(Env.getCtx()),Env.getAD_Org_ID(Env.getCtx()));
		if (!keep)
			return null;
		log.log(Level.INFO,
				"QSSLEC_FE_KEEP_WITHHOLDING_DOCNO_AFTER_GENERATED = Y");
		if (newWithholding) {
			withhodingNo = LEC_FE_MRetencion.generateWitholdingNo(iwh);
			invoice.set_ValueOfColumn("WithholdingNo", withhodingNo);
			invoice.saveEx();
		} else {
			withhodingNo = invoice.get_ValueAsString("WithholdingNo");
		}
		iwh.setDocumentNo(withhodingNo);
		iwh.saveEx();
		return withhodingNo;
	}
}
