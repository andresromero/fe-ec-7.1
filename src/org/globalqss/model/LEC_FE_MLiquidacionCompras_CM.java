package org.globalqss.model;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.sax.TransformerHandler;
import javax.xml.transform.stream.StreamResult;

import org.compiere.model.I_C_Location;
import org.compiere.model.MBPartner;
import org.compiere.model.MDocType;
import org.compiere.model.MInOut;
import org.compiere.model.MInvoice;
import org.compiere.model.MInvoiceLine;
import org.compiere.model.MInvoicePaySchedule;
import org.compiere.model.MLocation;
import org.compiere.model.MOrgInfo;
import org.compiere.model.MPaySchedule;
import org.compiere.model.MPaymentTerm;
import org.compiere.model.MSysConfig;
import org.compiere.model.Query;
import org.compiere.model.X_C_POSPayment;
import org.compiere.model.X_I_Conversion_Rate;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Trx;
import org.globalqss.util.LEC_FE_Utils;
import org.globalqss.util.LEC_FE_UtilsXml;
import org.xml.sax.helpers.AttributesImpl;

import ec.itsc.liquidacionCompras.DetalleImpuestos;
import ec.itsc.liquidacionCompras.Impuesto;
import ec.itsc.liquidacionCompras.InfoTributaria;
import ec.itsc.liquidacionCompras.LiquidacionCompra;
import ec.itsc.liquidacionCompras.MaquinaFiscal;
import ec.itsc.liquidacionCompras.ObligadoContabilidad;
import ec.itsc.liquidacionCompras.Pagos;
import ec.itsc.liquidacionCompras.Reembolsos;
import ec.itsc.liquidacionCompras.DetalleImpuestos.DetalleImpuesto;
import ec.itsc.liquidacionCompras.LiquidacionCompra.Detalles;
import ec.itsc.liquidacionCompras.LiquidacionCompra.InfoAdicional;
import ec.itsc.liquidacionCompras.LiquidacionCompra.InfoLiquidacionCompra;
import ec.itsc.liquidacionCompras.LiquidacionCompra.Detalles.Detalle;
import ec.itsc.liquidacionCompras.LiquidacionCompra.Detalles.Detalle.DetallesAdicionales;
import ec.itsc.liquidacionCompras.LiquidacionCompra.Detalles.Detalle.Impuestos;
import ec.itsc.liquidacionCompras.LiquidacionCompra.Detalles.Detalle.DetallesAdicionales.DetAdicional;
import ec.itsc.liquidacionCompras.LiquidacionCompra.InfoAdicional.CampoAdicional;
import ec.itsc.liquidacionCompras.LiquidacionCompra.InfoLiquidacionCompra.TotalConImpuestos;
import ec.itsc.liquidacionCompras.LiquidacionCompra.InfoLiquidacionCompra.TotalConImpuestos.TotalImpuesto;
import ec.itsc.liquidacionCompras.Pagos.Pago;
import ec.itsc.liquidacionCompras.Reembolsos.ReembolsoDetalle;


/**
 *	LEC_FE_MInvoice
 *
 *  @author Carlos Ruiz - globalqss - Quality Systems & Solutions - http://globalqss.com
 *  @version  $Id: LEC_FE_MInvoice.java,v 1.0 2014/05/06 03:37:29 cruiz Exp $
 */
public class LEC_FE_MLiquidacionCompras_CM extends MInvoice
{
	/**
	 *
	 */
	private static final long serialVersionUID = -924606040343895114L;

	private int		m_lec_sri_format_id = 0;
	//private int		m_inout_sus_id = 0;

	private String file_name = "";
	private String m_obligadocontabilidad = "NO";
	private String m_coddoc = "";
	private String m_accesscode;
	private String m_identificacionconsumidor = "";
	private String m_tipoidentificacioncomprador = "";
	private String m_identificacioncomprador = "";
	private String m_razonsocial = "";
	private boolean isInternal = false;
	private boolean IsGenerateInBatch = false;

	private BigDecimal m_totaldescuento = Env.ZERO;
	private BigDecimal m_totalbaseimponible = Env.ZERO;
	private BigDecimal m_totalvalorimpuesto = Env.ZERO;
	private BigDecimal m_sumadescuento = Env.ZERO;
	private BigDecimal m_sumabaseimponible = Env.ZERO;
	private BigDecimal m_sumavalorimpuesto = Env.ZERO;
	private String m_trxAutorizacionName = null;

	public LEC_FE_MLiquidacionCompras_CM(Properties ctx, int C_Invoice_ID, String trxName) {
		super(ctx, C_Invoice_ID, trxName);
	}

	public void addHeaderElement(TransformerHandler mmDoc, String att, String value, AttributesImpl atts) throws Exception {
		if (att != null) {
			mmDoc.startElement("","",att,atts);
			mmDoc.characters(value.toCharArray(),0,value.toCharArray().length);
			mmDoc.endElement("","",att);
		} else {
			throw new AdempiereUserError(att + " empty");
		}
	}

	
	  private static void jaxbObjectToXMLString(LiquidacionCompra lc) 
	    {
	        try
	        {
	            //Create JAXB Context
	            JAXBContext jaxbContext = JAXBContext.newInstance(LiquidacionCompra.class);
	             
	            //Create Marshaller
	            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
	 
	            //Required formatting??
	            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
	 
	            //Print XML String to Console
	            StringWriter sw = new StringWriter();
	             
	            //Write XML to StringWriter
	            jaxbMarshaller.marshal(lc, sw);
	             
	            //Verify XML Content
	            String xmlContent = sw.toString();
	            System.out.println( xmlContent );
	 
	        } catch (JAXBException e) {
	            e.printStackTrace();
	        }
	    }
	  
	   private static void jaxbObjectToXMLFile(LiquidacionCompra lc, File file) 
	    {
	        try
	        {
	            //Create JAXB Context
	            JAXBContext jaxbContext = JAXBContext.newInstance(lc.getClass());
	             
	            //Create Marshaller
	            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
	 
	            //Required formatting??
	            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
	             
	            //Writes XML file to file-system
	            jaxbMarshaller.marshal(lc, file); 
	        } 
	        catch (JAXBException e) 
	        {
	            e.printStackTrace();
	        }
	    }



	public String lecfeinv_SriExportLiquidacionCompraXML100 ()
	{
		Trx autorizacionTrx = null;
		m_trxAutorizacionName=null;
		int autorizationID = 0;
		String msg = null;
		
		LEC_FE_UtilsXml signature = new LEC_FE_UtilsXml(getAD_Client_ID(), getAD_Org_ID());

		try
		{

		signature.setAD_Org_ID(getAD_Org_ID());

		autorizationID = get_ValueAsInt("sri_authorization_invoice_ID");		
		if(autorizationID!=0){//condicion para asegurarse que tiene una autorizacion correectamente configurada
			X_SRI_Authorization a = new X_SRI_Authorization(getCtx(), autorizationID, autorizacionTrx!=null?autorizacionTrx.getTrxName():get_TrxName());
			if(a.getValue()==null || a.getValue().length()<10 || a.getDescription()==null || !a.getDescription().contains(getDocumentNo()))
				autorizationID = 0;
		}
		
		m_identificacionconsumidor=MSysConfig.getValue("QSSLEC_FE_IdentificacionConsumidorFinal", null, getAD_Client_ID(), getAD_Org_ID());

		m_razonsocial=MSysConfig.getValue("QSSLEC_FE_RazonSocialPruebas", null, getAD_Client_ID(), getAD_Org_ID());

		signature.setPKCS12_Resource(MSysConfig.getValue("QSSLEC_FE_RutaCertificadoDigital", null, getAD_Client_ID(), getAD_Org_ID()));
		signature.setPKCS12_Password(MSysConfig.getValue("QSSLEC_FE_ClaveCertificadoDigital", null, getAD_Client_ID(), getAD_Org_ID()));

		if (signature.getFolderRaiz() == null)
			return "Error en Factura No "+getDocumentNo()+" No existe parametro para Ruta Generacion Xml";

		MDocType dt = new MDocType(getCtx(), getC_DocTypeTarget_ID(), get_TrxName());
		if (dt.get_Value("IsInternal")!=null)
			isInternal = dt.get_ValueAsBoolean("IsInternal");

		m_coddoc = dt.get_ValueAsString("SRI_ShortDocType");
		if(m_coddoc!=null && m_coddoc.length()>2){
			String[] s = m_coddoc.split("-");
			m_coddoc = s[s.length-1];
		}

		if ( m_coddoc.equals(""))
			return "Error en Liquidaci�n de Compras No "+getDocumentNo()+" No existe definicion SRI_ShortDocType: " + dt.toString();

		// Formato
		m_lec_sri_format_id = LEC_FE_Utils.getLecSriFormat(getAD_Client_ID(), signature.getDeliveredType(), m_coddoc, getDateInvoiced(), getDateInvoiced());

		if ( m_lec_sri_format_id < 1)
			return "Error en Liquidaci�n de Compras No "+getDocumentNo()+" No existe formato para el comprobante";

		X_LEC_SRI_Format f = new X_LEC_SRI_Format (getCtx(), m_lec_sri_format_id, get_TrxName());

		// Emisor
		MOrgInfo oi = MOrgInfo.get(getCtx(), getAD_Org_ID(), get_TrxName());

		msg = LEC_FE_ModelValidator.valideOrgInfoSri (oi);

		if (msg != null)
			return "Error en Factura No "+getDocumentNo()+" "+msg;

		if ( (Boolean) oi.get_Value("SRI_IsKeepAccounting"))
			m_obligadocontabilidad = "SI";

		int c_bpartner_id = LEC_FE_Utils.getOrgBPartner(getAD_Client_ID(), oi.get_ValueAsString("TaxID"));
		MBPartner bpe = new MBPartner(getCtx(), c_bpartner_id, get_TrxName());

		MLocation lo = new MLocation(getCtx(), oi.getC_Location_ID(), get_TrxName());

		int c_location_matriz_id = MSysConfig.getIntValue("QSSLEC_FE_LocalizacionDireccionMatriz", -1, oi.getAD_Client_ID(), oi.getAD_Org_ID());

		MLocation lm = new MLocation(getCtx(), c_location_matriz_id, get_TrxName());

		// Comprador
		MBPartner bp = new MBPartner(getCtx(), getC_BPartner_ID(), get_TrxName());
		if (!signature.isOnTesting()) m_razonsocial = bp.getName();

		X_LCO_TaxIdType ttc = new X_LCO_TaxIdType(getCtx(), (Integer) bp.get_Value("LCO_TaxIdType_ID"), get_TrxName());

		m_tipoidentificacioncomprador = LEC_FE_Utils.getTipoIdentificacionSri(ttc.get_Value("LEC_TaxCodeSRI").toString());

		m_identificacioncomprador = bp.getTaxID();

		X_LCO_TaxIdType tt = new X_LCO_TaxIdType(getCtx(), (Integer) bp.get_Value("LCO_TaxIdType_ID"), get_TrxName());
		if (tt.getLCO_TaxIdType_ID() == 1000011)	// Hardcoded F Final	// TODO Deprecated
			m_identificacioncomprador = m_identificacionconsumidor;

		/*m_inout_sus_id = LEC_FE_Utils.getInvoiceDocSustento(getC_Invoice_ID());

		if ( m_inout_sus_id < 1)
			log.warning("No existe documento sustento para el comprobante (Entrega)");	// TODO Reviewme
			// throw new AdempiereUserError("No existe documento sustento para el comprobante");

		MInOut inoutsus = null;
		if ( m_inout_sus_id > 0)
			inoutsus = new MInOut(getCtx(), m_inout_sus_id, get_TrxName());*/

		// Support ticket http://support.ingeint.com/issues/727
		boolean descuentoProaudio = MSysConfig.getBooleanValue("ITSC_DiscountProaudio", false, Env.getAD_Client_ID(Env.getCtx()), Env.getAD_Org_ID(Env.getCtx()));
		//(Select DiscountAMT from c_orderline where c_orderline_ID=cil.c_orderline_ID)
		String campoDescuento = "il.DiscountAmt";
		if(descuentoProaudio)//CAMBIO PARA PROAUDIO CAMC
			campoDescuento = "(Select coalesce(DiscountAMT,0) from c_orderline where c_orderline_ID=il.c_orderline_ID)";
		
		m_totaldescuento = DB.getSQLValueBD(get_TrxName(), "SELECT COALESCE(SUM("+campoDescuento+"), 0) FROM c_invoiceLine il WHERE il.C_Invoice_ID = ? ", getC_Invoice_ID());

		//
		X_SRI_Authorization a = null;//CAMC
		X_SRI_AccessCode ac = null;//CAMC
		if(autorizationID!=0){//condicion para tomar autorizacion/clave de acceso previamente creada en la factura
			a = new X_SRI_Authorization(getCtx(), autorizationID, autorizacionTrx!=null?autorizacionTrx.getTrxName():get_TrxName());
			if(a.getSRI_AccessCode()!=null)
				ac = new X_SRI_AccessCode(getCtx(), a.getSRI_AccessCode_ID(), autorizacionTrx!=null?autorizacionTrx.getTrxName():get_TrxName());
		}
		int sri_accesscode_id = 0;

		// New/Upd Access Code

		//Se crea una transaccion nueva para la autorizacion y clave de acceso, para realizar un commit, en el
		//caso de que se envie el comprobante y no se obtenga respuesta del SRI
		if (m_trxAutorizacionName == null)
		{
			StringBuilder l_trxname = new StringBuilder("SRI_")
				.append(get_TableName()).append(get_ID());
			m_trxAutorizacionName = Trx.createTrxName(l_trxname.toString());
			autorizacionTrx = Trx.get(m_trxAutorizacionName, true);
		}
		if(ac == null){//si ya existe clave de acceso ya no creo otra para tomarla//CAMC
			ac = new X_SRI_AccessCode (getCtx(), sri_accesscode_id,autorizacionTrx!=null?autorizacionTrx.getTrxName():get_TrxName());
			ac.setAD_Org_ID(getAD_Org_ID());
			ac.setOldValue(null);	// Deprecated
			ac.setEnvType(signature.getEnvType());
			ac.setCodeAccessType(signature.getCodeAccessType());
			ac.setSRI_ShortDocType(m_coddoc);
			ac.setIsUsed(true);//a�adir on/offline //CAMC
	
			// Access Code
			//m_accesscode = LEC_FE_Utils.getAccessCode(getDateInvoiced(), m_coddoc, bpe.getTaxID(), oi.get_ValueAsString("SRI_OrgCode"), LEC_FE_Utils.getStoreCode(LEC_FE_Utils.formatDocNo(getDocumentNo(), m_coddoc)), getDocumentNo(), oi.get_ValueAsString("SRI_DocumentCode"), signature.getDeliveredType(), ac);
			//CAMC ahora toma el primer tramo del numero de documento para el establecimiento, ya no lo saca de la org
			m_accesscode = LEC_FE_Utils.getAccessCode(getDateInvoiced(), m_coddoc, bpe.getTaxID(), LEC_FE_Utils.getEstablecimientoCode(LEC_FE_Utils.formatDocNo(getDocumentNo(), m_coddoc)), LEC_FE_Utils.getStoreCode(LEC_FE_Utils.formatDocNo(getDocumentNo(), m_coddoc)), getDocumentNo(), oi.get_ValueAsString("SRI_DocumentCode"), signature.getDeliveredType(), ac);
	
			if (signature.getCodeAccessType().equals(LEC_FE_UtilsXml.claveAccesoAutomatica))
				ac.setValue(m_accesscode);
	
			if (!ac.save()) {
				msg = "@SaveError@ No se pudo grabar SRI Access Code";
				return "Error en Factura No "+getDocumentNo()+" "+msg;
			}
		}else
			m_accesscode = ac.getValue();
		// New Authorization

		if(a == null){//si ya existe autorizacion, ya no creo otra para tomarla//CAMC
			a = new X_SRI_Authorization (getCtx(), 0, autorizacionTrx!=null?autorizacionTrx.getTrxName():get_TrxName());
			a.setAD_Org_ID(getAD_Org_ID());
			a.setSRI_ShortDocType(m_coddoc);
			a.setValue(m_accesscode);
			a.setSRI_AccessCode_ID(ac.get_ID());
			a.setSRI_ErrorCode_ID(0);
			a.setAD_UserMail_ID(getAD_User_ID());
			
			if(signature.isOffline){//setear la autorizacion con el mismo numero de la clave de acceso en caso de ser offline//setear (no) recibido//CAMC
				a.setSRI_AuthorizationCode(m_accesscode);
				a.setDescription(getDocumentNo());
				a.set_ValueOfColumn("isSRIOfflineSchema", true);
			}
			
			if (!a.save()) {
				msg = "@SaveError@ No se pudo grabar SRI Autorizacion";
				return "Error en Factura No "+getDocumentNo()+" "+msg;
			}
		}else if (a.getSRI_AccessCode()==null){
			a.setSRI_AccessCode_ID(ac.get_ID());
			if (!a.save()) {
				msg = "@SaveError@ No se pudo grabar SRI Autorizacion";
				return "Error en Factura No "+getDocumentNo()+" "+msg;
			}
		} 
		
		autorizationID = a.get_ID();

		String xmlFileName = "SRI_" + m_coddoc + "-" + LEC_FE_Utils.getDate(getDateInvoiced(),9) + "-" + m_accesscode + ".xml";

		//ruta completa del archivo xml
		file_name = signature.getFolderRaiz() + File.separator + LEC_FE_UtilsXml.folderComprobantesGenerados + File.separator + xmlFileName;
		//Stream para el documento xml
		
				
		

		StringBuffer sql = null;
		

		LiquidacionCompra lc = new LiquidacionCompra();
		lc.setVersion("1.1.0");
		lc.setId("comprobante");
		InfoTributaria it = new InfoTributaria();
		{
			it.setAmbiente(signature.getEnvType());
			it.setTipoEmision(signature.getDeliveredType());
			it.setRazonSocial(bpe.getName());
			if(bpe.getName2()!=null && !bpe.getName2().equalsIgnoreCase(""))
				it.setNombreComercial(bpe.getName2());
			it.setRuc((LEC_FE_Utils.fillString(13 - (LEC_FE_Utils.cutString(bpe.getTaxID(), 13)).length(), '0'))
					+ LEC_FE_Utils.cutString(bpe.getTaxID(),13));
			it.setClaveAcceso(a.getValue());
			it.setCodDoc(m_coddoc);
			it.setEstab(LEC_FE_Utils.getEstablecimientoCode(LEC_FE_Utils.formatDocNo(getDocumentNo(), m_coddoc)));
			it.setPtoEmi(LEC_FE_Utils.getStoreCode(LEC_FE_Utils.formatDocNo(getDocumentNo(), m_coddoc)));
			it.setSecuencial((LEC_FE_Utils.fillString(9 - (LEC_FE_Utils.cutString(LEC_FE_Utils.getSecuencial(getDocumentNo(), m_coddoc), 9)).length(), '0'))
					+ LEC_FE_Utils.cutString(LEC_FE_Utils.getSecuencial(getDocumentNo(), m_coddoc), 9));
			it.setDirMatriz(lm.getAddress1());
			lc.setInfoTributaria(it);
		}
		

		
		InfoLiquidacionCompra ilc = new InfoLiquidacionCompra();
		{
			ilc.setFechaEmision(LEC_FE_Utils.getDate(getDateInvoiced(),10));
			ilc.setDirEstablecimiento(lo.getAddress1());//Opcional
			if(oi.get_ValueAsString("SRI_TaxPayerCode")!=null && !oi.get_ValueAsString("SRI_TaxPayerCode").equalsIgnoreCase(""))
				ilc.setContribuyenteEspecial(oi.get_ValueAsString("SRI_TaxPayerCode"));//Opcional
			ilc.setObligadoContabilidad((Boolean)oi.get_Value("SRI_IsKeepAccounting")?ObligadoContabilidad.SI:ObligadoContabilidad.NO);//Opcionl
			ilc.setTipoIdentificacionProveedor(m_tipoidentificacioncomprador);//Opcional conforme tabla 6
			ilc.setRazonSocialProveedor(m_razonsocial);
			ilc.setIdentificacionProveedor(m_identificacioncomprador);

			MLocation loBpBill = (MLocation) getC_BPartner_Location().getC_Location();
			if (loBpBill.getAddress1()==null)
				ilc.setDireccionProveedor(LEC_FE_Utils.cutString(loBpBill.getAddress1().toString(),300));//Opcional
			
			ilc.setTotalSinImpuestos(getTotalLines());
			ilc.setTotalDescuento(m_totaldescuento);
			//ilc.setCodDocReembolso("00");/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//ilc.setTotalComprobantesReembolso(new BigDecimal(50.00));/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//ilc.setTotalBaseImponibleReembolso(new BigDecimal(50.00));////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//ilc.setTotalImpuestoReembolso(new BigDecimal(50.00));/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			TotalConImpuestos tci = new TotalConImpuestos();
			{
				

				sql = new StringBuffer(
			             "SELECT COALESCE(tc.SRI_TaxCodeValue, '0') AS codigo "
			             + ", COALESCE(tr.SRI_TaxRateValue, 'X') AS codigoPorcentaje "
						 + ", SUM(it.TaxBaseAmt) AS baseImponible "
						 + ", SUM(it.TaxAmt) AS valor "
						 + ", 0::numeric AS descuentoAdicional "
						 + ", t.rate AS tarifa "
						 + "FROM C_Invoice i "
						 + "JOIN C_InvoiceTax it ON it.C_Invoice_ID = i.C_Invoice_ID "
						 + "JOIN C_Tax t ON t.C_Tax_ID = it.C_Tax_ID AND LEC_TaxTypeSRI='2' "
						 + "JOIN C_TaxCategory ct ON ct.C_TaxCategory_ID = t.C_TaxCategory_ID AND ct.IsWithholding='N' "
						 + "LEFT JOIN SRI_TaxCode tc ON t.SRI_TaxCode_ID = tc.SRI_TaxCode_ID "
						 + "LEFT JOIN SRI_TaxRate tr ON t.SRI_TaxRate_ID = tr.SRI_TaxRate_ID "
						 + "WHERE i.C_Invoice_ID = ? "
						 + "GROUP BY codigo, codigoPorcentaje, tarifa "
						 + "ORDER BY codigo, codigoPorcentaje");
				
				PreparedStatement pstmt = null;
				ResultSet rs = null;
				
				try
				{
					pstmt = DB.prepareStatement(sql.toString(), get_TrxName());
					pstmt.setInt(1, getC_Invoice_ID());
					rs = pstmt.executeQuery();

					while (rs.next())
					{
						//iterable ya que pueden ser varios impuestos
						TotalImpuesto ti = new TotalImpuesto();
						{
							ti.setCodigo(rs.getString(1));
							ti.setCodigoPorcentaje(rs.getString(2));
							ti.setDescuentoAdicional(rs.getBigDecimal(5));
							ti.setBaseImponible(rs.getBigDecimal(3));
							ti.setTarifa(rs.getBigDecimal(6));///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
							ti.setValor(rs.getBigDecimal(4));
							tci.getTotalImpuesto().add(ti);
						}
						m_totalbaseimponible = m_totalbaseimponible.add(rs.getBigDecimal(3));
						m_totalvalorimpuesto = m_totalvalorimpuesto.add(rs.getBigDecimal(4));
						ilc.setTotalConImpuestos(tci);
						
					}

				}catch (SQLException e)
				{
					log.log(Level.SEVERE, sql.toString(), e);
					msg = "Error SQL: " + sql.toString();
					return "Error en Liquidaci�n de Compra No "+getDocumentNo()+" "+msg;
				}
				finally{
					if(rs!=null)rs.close();
					if(pstmt!=null)pstmt.close();
				}
			}
			ilc.setImporteTotal(getGrandTotal());
			ilc.setMoneda(getCurrencyISO());
			Pagos p = new Pagos();
			{

				MInvoicePaySchedule[] ipss = MInvoicePaySchedule.getInvoicePaySchedule(getCtx(), get_ID(), 0, get_TrxName());
				MInvoice invoice = new MInvoice(getCtx(),get_ID(),get_TrxName());

				if (ipss.length == 0){
					MPaymentTerm paymentTerm = (MPaymentTerm) getC_PaymentTerm();
					Pago pp = new Pago();
					{
						pp.setFormaPago(invoice.get_ValueAsString("LEC_PaymentMethod"));
						pp.setTotal(getGrandTotal());
						pp.setPlazo(new BigDecimal(paymentTerm.getNetDays()));
						pp.setUnidadTiempo("dias");
						p.getPago().add(pp);
					}
					ilc.setPagos(p);
				}
				else{
					for(MInvoicePaySchedule ips : ipss){
						//iterable ya que pueden ser varios pagos
						Pago pp = new Pago();
						{
							pp.setFormaPago(ips.get_ValueAsString("LEC_PaymentMethod"));
							pp.setTotal(ips.getDueAmt());
							pp.setPlazo(new BigDecimal((ips.getDueDate().getTime() - invoice.getDateAcct().getTime()) / (1000*60*60*24)));
							pp.setUnidadTiempo("dias");
							p.getPago().add(pp);
						}
						ilc.setPagos(p);
					}
				}
			}
			lc.setInfoLiquidacionCompra(ilc);
		} 
		
		
		
		Detalles d = new Detalles();
		{
			sql = new StringBuffer(
		            "SELECT i.C_Invoice_ID, COALESCE(p.value, '0'), COALESCE(p.UPC,p.value,0::text), ilt.name, COALESCE(ilt.QtyEntered,0), ROUND(COALESCE(ilt.PriceEntered,0),6), COALESCE("+campoDescuento+",0) AS discount, (COALESCE(ilt.linenetamt,0) - COALESCE("+campoDescuento+",0)) as linenetamt  "
					+ ", COALESCE(tc.SRI_TaxCodeValue, '0') AS codigo "
					+ ", COALESCE(tr.SRI_TaxRateValue, 'X') AS codigoPorcentaje "
					+ ", t.rate AS tarifa "
					+ ", (COALESCE(ilt.linenetamt,0) - COALESCE("+campoDescuento+",0)) AS baseImponible "
					+ ", ROUND((COALESCE(ilt.linenetamt,0) - COALESCE("+campoDescuento+",0)) * t.rate / 100, 2) AS valor  "
					+ ", il.description AS description1 "
					+ ", 0::numeric AS descuentoAdicional "
					+ ", il.C_UOM_ID "
					+ ", il.Description "
					+ ", il.C_InvoiceLine_ID "
		            + "FROM C_Invoice i "
		            + "JOIN C_InvoiceLine il ON il.C_Invoice_ID = i.C_Invoice_ID "
		            + "JOIN C_Invoice_LineTax_VT ilt ON ilt.C_Invoice_ID = i.C_Invoice_ID AND ilt.C_InvoiceLine_ID = il.C_InvoiceLine_ID  and ilt.ad_language='es_EC'"//Ultimo and añadido por CAMC para no tener datos repetidos por Colombia
		            + "JOIN C_Tax t ON t.C_Tax_ID = ilt.C_Tax_ID "
		            + "LEFT JOIN SRI_TaxCode tc ON t.SRI_TaxCode_ID = tc.SRI_TaxCode_ID "
					+ "LEFT JOIN SRI_TaxRate tr ON t.SRI_TaxRate_ID = tr.SRI_TaxRate_ID "
		            + "LEFT JOIN M_Product p ON p.M_Product_ID = il.M_Product_ID "
		            + "LEFT JOIN M_Product_Category pc ON pc.M_Product_Category_ID = p.M_Product_Category_ID "
		            + "LEFT JOIN C_Charge c ON il.C_Charge_ID = c.C_Charge_ID AND c.IsInternal ='N' "
		            + "WHERE il.IsDescription = 'N' AND i.C_Invoice_ID=? and il.linenetamt>=0 "
		            + "ORDER BY il.line");

			PreparedStatement pstmt = null;
			ResultSet rs = null;
			
			try
			{
				pstmt = DB.prepareStatement(sql.toString(), get_TrxName());
				pstmt.setInt(1, getC_Invoice_ID());
				rs = pstmt.executeQuery();

				//iterable ya que pueden ser varios Detalles
				while(rs.next()){
					Detalle dd = new Detalle();
					dd.setCodigoPrincipal(LEC_FE_Utils.cutString(rs.getString(2),25));
					dd.setCodigoAuxiliar(LEC_FE_Utils.cutString(rs.getString(3),25));
					if(rs.getString(4)==null || rs.getString(4).length()==0)
						throw new Exception("Definir Producto, Cargo o Descripci�n en todas las lineas de la Factura.");
					dd.setDescripcion(LEC_FE_Utils.cutString(rs.getString(4),300));

					String ad_language = Env.getAD_Language(getCtx());
					boolean isBaseLanguage = Env.isBaseLanguage(ad_language, "AD_Ref_List");
					String UOMTable = isBaseLanguage ?"C_UOM":"C_UOM_Trl";
					Object UOMNameOb = new Query(getCtx(),UOMTable,"C_UOM_ID = ?",null).setParameters(rs.getBigDecimal(16)).first().get_ValueAsString("Name");
					if (UOMNameOb!=null)
						dd.setUnidadMedida(LEC_FE_Utils.cutString(UOMNameOb.toString(),50));
					
					dd.setCantidad(rs.getBigDecimal(5));
					dd.setPrecioUnitario(rs.getBigDecimal(6));
					dd.setDescuento(rs.getBigDecimal(7));
					dd.setPrecioTotalSinImpuesto(rs.getBigDecimal(8));
					DetallesAdicionales da = new DetallesAdicionales();
					{

						List<List<Object>> adicionales = DB.getSQLArrayObjectsEx(get_TrxName(), "Select name, description from C_InvoiceLineAddDetail where c_invoiceline_id = ? order by created", rs.getInt(18));	
						if(adicionales!=null && adicionales.size()>0){
							for(List<Object> adicional : adicionales)
								da.getDetAdicional().add(new DetAdicional(LEC_FE_Utils.cutString(adicional.get(0)+"",300), LEC_FE_Utils.cutString(adicional.get(1)+"",300)));
							dd.setDetallesAdicionales(da);
						}
					}
					Impuestos i = new Impuestos();
					{
						if (rs.getString(9).equals("0") || rs.getString(10).equals("X") ) {
							msg = "Impuesto sin Tipo Porcentaje impuesto SRI";
							return "Error en Factura No "+getDocumentNo()+" "+msg;
						}
						//iterable ya que pueden ser varios Impuestos
						Impuesto ii = new Impuesto();
						{
							ii.setCodigo(rs.getString(9));
							ii.setCodigoPorcentaje(rs.getString(10));
							ii.setTarifa(rs.getBigDecimal(11));
							ii.setBaseImponible(rs.getBigDecimal(12));
							ii.setValor(rs.getBigDecimal(13));
							i.getImpuesto().add(ii);
						}
						dd.setImpuestos(i);
					}
					d.getDetalle().add(dd);
					m_sumadescuento = m_sumadescuento.add(rs.getBigDecimal(7));
					m_sumabaseimponible = m_sumabaseimponible.add(rs.getBigDecimal(12));
					m_sumavalorimpuesto = m_sumavalorimpuesto.add(rs.getBigDecimal(13));
				}
				lc.setDetalles(d);
			}catch (SQLException e)
			{
				log.log(Level.SEVERE, sql.toString(), e);
				msg = "Error SQL: " + sql.toString();
				return "Error en Liquidaci�n de Compra No "+getDocumentNo()+" "+msg;
			}
			finally{
				if(rs!=null)rs.close();
				if(pstmt!=null)pstmt.close();
			}
		}
		/*
		Reembolsos r = new Reembolsos();//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		{
			ReembolsoDetalle rd = new ReembolsoDetalle();
			{
				rd.setTipoIdentificacionProveedorReembolso("04");
				rd.setIdentificacionProveedorReembolso("identificacionProvee");
				rd.setCodPaisPagoProveedorReembolso("000");
				rd.setTipoProveedorReembolso("01");
				rd.setCodDocReembolso("00");
				rd.setEstabDocReembolso("000");
				rd.setPtoEmiDocReembolso("000");
				rd.setSecuencialDocReembolso("000000000");
				rd.setFechaEmisionDocReembolso("01/01/2000");
				rd.setNumeroautorizacionDocReemb("0000000000");
				DetalleImpuestos di = new DetalleImpuestos();
				{
					//iterable ya que pueden ser varios Detalles Impuestos
					DetalleImpuesto dii = new DetalleImpuesto();
					{
						dii.setCodigo("02");
						dii.setCodigoPorcentaje("0");
						dii.setTarifa("0");
						dii.setBaseImponibleReembolso(new BigDecimal(50.00));
						dii.setImpuestoReembolso(new BigDecimal(50.00));
						di.getDetalleImpuesto().add(dii);
					}
					rd.setDetalleImpuestos(di);
				}
				r.getReembolsoDetalle().add(rd);
			}
			lc.setReembolsos(r);
		}
		MaquinaFiscal mf = new MaquinaFiscal();///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		{
			mf.setMarca("SISPAU");
			mf.setModelo("ABC1234");
			mf.setSerie("CGMC1405");
			lc.setMaquinaFiscal(mf);
		}*/
		InfoAdicional ia = new InfoAdicional();
		{
			//iterable ya que pueden ser varios Campos Adicionales
			if(getPOReference()!=null && !getPOReference().isEmpty())
				ia.getCampoAdicional().add(new CampoAdicional("Ref. Orden", getPOReference()));
			if(getDescription()!=null && !getDescription().isEmpty())
				ia.getCampoAdicional().add(new CampoAdicional("Descripcion", getDescription()));
			if(getAD_User()!=null && getAD_User().getPhone()!=null && !getAD_User().getPhone().isEmpty())
				ia.getCampoAdicional().add(new CampoAdicional("Telefono", getAD_User().getPhone()));
			if(getAD_User()!=null && getAD_User().getEMail()!=null && !getAD_User().getEMail().isEmpty())
				ia.getCampoAdicional().add(new CampoAdicional("EMail", getAD_User().getEMail()));
			if(getC_BPartner_Location()!=null && getC_BPartner_Location().getC_Location()!=null){
				I_C_Location loc = getC_BPartner_Location().getC_Location();
				if(loc.getAddress1()!=null && !loc.getAddress1().isEmpty()){
					String dir = loc.getAddress1();
					if(loc.getAddress2()!=null && !loc.getAddress2().isEmpty())
						dir += ", "+loc.getAddress2();
					if(loc.getC_City()!=null && loc.getC_City().getName()!=null && !loc.getC_City().getName().isEmpty())
						dir += ", "+loc.getC_City().getName();
					if(loc.getC_Region()!=null && loc.getC_Region().getName()!=null && !loc.getC_Region().getName().isEmpty())
						dir += ", "+loc.getC_Region().getName();
					if(loc.getC_Country()!=null && loc.getC_Country().getName()!=null && !loc.getC_Country().getName().isEmpty())
						dir += ", "+loc.getC_Country().getName();
					ia.getCampoAdicional().add(new CampoAdicional("Direccion", dir));
				}
			}
			
			lc.setInfoAdicional(ia);
		}
		
		

			/**********************************************************************************************************************/


		if (m_sumadescuento.compareTo(m_totaldescuento) != 0 ) {
			msg = "Error Diferencia Descuento Total: " + m_totaldescuento.toString() + " Detalles: " + m_sumadescuento.toString();
			return "Error en Factura No "+getDocumentNo()+" "+msg;
		}

		if (m_sumabaseimponible.compareTo(m_totalbaseimponible) != 0
			&& m_totalbaseimponible.subtract(m_sumabaseimponible).abs().compareTo(LEC_FE_UtilsXml.HALF) > 1) {
			msg = "Error Diferencia Base Impuesto Total: " + m_totalbaseimponible.toString() + " Detalles: " + m_sumabaseimponible.toString();
			return "Error en Factura No "+getDocumentNo()+" "+msg;
		}

		if (m_sumavalorimpuesto.compareTo(m_totalvalorimpuesto) != 0
			&& m_totalvalorimpuesto.subtract(m_sumavalorimpuesto).abs().compareTo(LEC_FE_UtilsXml.HALF) > 1) {
			msg = "Error Diferencia Impuesto Total: " + m_totalvalorimpuesto.toString() + " Detalles: " + m_sumavalorimpuesto.toString();
			return "Error en Factura No "+getDocumentNo()+" "+msg;
		}
		
		jaxbObjectToXMLFile(lc, new File(file_name));

//		if (LEC_FE_Utils.breakDialog("Firmando Xml")) return "Cancelado...";	// TODO Temp

		//log.warning("@Signing Xml@ -> " + file_name);
		signature.setResource_To_Sign(file_name);
		signature.setOutput_Directory(signature.getFolderRaiz() + File.separator + LEC_FE_UtilsXml.folderComprobantesFirmados);
        signature.execute();

        file_name = signature.getFilename(signature, LEC_FE_UtilsXml.folderComprobantesFirmados);

        if (dt.get_Value("IsGenerateInBatch")!=null)
        	IsGenerateInBatch =dt.get_ValueAsBoolean("IsGenerateInBatch");
        //if ((!signature.IsUseContingency && !IsGenerateInBatch)) {//CAMC
        if ((!signature.IsUseContingency && !IsGenerateInBatch && !signature.isOffline()) ||//CAMC
        	(getDocStatus().equalsIgnoreCase(DOCSTATUS_Completed) && a.get_ValueAsBoolean("isSRIOfflineSchema"))) {//Si es sitema offline y la factura ya fue completa anteriormente, se envia al sri//CAMC

//	        if (LEC_FE_Utils.breakDialog("Enviando Comprobante al SRI")) return "Cancelado...";	// TODO Temp

	        // Procesar Recepcion SRI
	        //log.warning("@Sending Xml@ -> " + file_name);
	        msg = signature.respuestaRecepcionComprobante(file_name);

	        if (msg != null)
	        	if (!msg.equals("RECIBIDA") && !msg.contains("-43-")){
		        //if (!msg.equals("RECIBIDA") || (msg.contains("-43-"))){//CAMC //Dejar pasar si sale error 43, clave ya registrada.
	        		if (autorizacionTrx!=null){
	        			autorizacionTrx.rollback();
	        			autorizacionTrx.close();
	        			autorizacionTrx = null;
	        		}
	    	        log.warning("Error en Factura No "+getDocumentNo()+" "+msg);
	    	        //if(!signature.isOffline)//CAMC //Dejar pasar las offline porque ya tienen grabada la autorizacion
	    	        	return "Error en Factura No "+getDocumentNo()+" "+msg;
	        	}
	        String invoiceNo = getDocumentNo();
	        String invoiceID = String.valueOf(get_ID());
	        a.setDescription(invoiceNo);
	        a.set_ValueOfColumn("DocumentID", invoiceID);
	        a.saveEx();
        	if (autorizacionTrx!=null){
        		autorizacionTrx.commit(true);
        	}
	        // Procesar Autorizacion SRI
	        //log.warning("@Authorizing Xml@ -> " + file_name);
	        try {
	        	msg = signature.respuestaAutorizacionComprobante(ac, a, m_accesscode);

	        	if (msg != null){
	        		if (autorizacionTrx!=null){
	            		autorizacionTrx.commit();
	    				autorizacionTrx.close();
	    				autorizacionTrx = null;
	            	}
	        		return "Error en Factura No "+getDocumentNo()+" "+msg;
	        	}

	        } catch (Exception ex) {
	        	// Completar en estos casos, luego usar Boton Reprocesar Autorizacion
	        	// 70-Clave de acceso en procesamiento
	        	if (a.getSRI_ErrorCode().getValue().equals("70"))
		        	// ignore exceptions
		        	log.warning(msg + ex.getMessage());
	        	else
	        		return msg;
	        }

		    file_name = signature.getFilename(signature, LEC_FE_UtilsXml.folderComprobantesAutorizados);
		} else {	// emisionContingencia
			// Completar en estos casos, luego usar Boton Procesar
			// 170-Clave de contingencia pendiente
			if (signature.IsUseContingency)
				a.setSRI_ErrorCode_ID(LEC_FE_Utils.getErrorCode("170"));
			// 70-Clave de acceso en procesamiento
			if (IsGenerateInBatch || signature.isOffline())
				a.setSRI_ErrorCode_ID(LEC_FE_Utils.getErrorCode("70"));
    		a.saveEx();

    		if (signature.isAttachXml())
        		LEC_FE_Utils.attachXmlFile(a.getCtx(), a.get_TrxName(), a.getSRI_Authorization_ID(), file_name);
        	if (autorizacionTrx!=null){
        		autorizacionTrx.commit(true);

        	}
		}
        //log.warning("@SRI_FileGenerated@ -> " + file_name);
		if (autorizacionTrx!=null){
    		autorizacionTrx.commit();

    	}


//		if (LEC_FE_Utils.breakDialog("Completando Factura")) return "Cancelado...";	// TODO Temp

		//
		}
		catch (Exception e)
		{
			log.warning("Error: "+e.getMessage());
			log.warning("localized: "+e.getLocalizedMessage());
			e.printStackTrace();
			msg = "No se pudo crear XML - " + e.getMessage();
			log.severe(msg);
			if (autorizacionTrx!=null){
        		autorizacionTrx.commit();

					autorizacionTrx.close();
					autorizacionTrx = null;

        	}
			return "Error en Liquidaci�n de Compra No "+getDocumentNo()+" "+msg;
		}catch (Error e) {
			msg = "No se pudo crear XML- Error en Conexion con el SRI";
			return "Error en Liquidaci�n de Compra No "+getDocumentNo()+" "+msg;
		}
		if (autorizacionTrx!=null){
  			autorizacionTrx.close();
			autorizacionTrx = null;
    	}
		//set_Value("SRI_Authorization_ID",autorizationID);
		set_Value("sri_authorization_invoice_ID",autorizationID);
		this.saveEx();
		return msg;

	} // lecfeinv_SriExportInvoiceXML100


}	// LEC_FE_MLiquidacionCompras
